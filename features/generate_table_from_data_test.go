package psv_test

import (
	"fmt"

	"codeberg.org/japh/psv"
)

// ExampleTable_Encode_generate_table_from_data demonstrates how to generate a
// PSV table from a 2-dimensional slice of go strings.
//
// See also ExampleTable_Encode_generate_table_from_table (generate_table_from_table_test.go)
func ExampleTable_Encode_generate_table_from_data() {

	data := [][]string{
		{"name", "region", "popularity", "variations", "difficulty"},
		{"Schnitzel", "Germany", "5", "1", "5"},
		{"Pizza", "Italy", "4", "100", "2"},
		{"Spaghetti", "Italy", "3", "4", "1"},
		{"Quiche", "France", "1", "10", "1"},
		{"Fried Rice", "Asia", "3", "50", "1"},
		{"Chicken Jalfrezi", "India", "3", "3", "4"},
	}

	deco := []*psv.TextLine{
		{Line: 1},
		{Line: 2, Ruler: "+--"},
		{Line: 4, Ruler: "| ="},
		{Line: 5},
		{Line: 6, Text: "Classic European:"},
		{Line: 11},
		{Line: 12, Text: "Exotic:"},
		{Line: 15, Ruler: "+--"},
		{Line: 16},
		{Line: 17, Text: "        Note: 72% of these statistics are probably 86% inaccurate,"},
		{Line: 18, Text: "              or just completely made up :-)"},
	}

	yumminess := psv.NewTable()
	yumminess.Data = data
	yumminess.TextLines = deco

	fmt.Print(yumminess.Encode())

	// output:
	//
	// +------------------+---------+------------+------------+------------+
	// | name             | region  | popularity | variations | difficulty |
	// | ================ | ======= | ========== | ========== | ========== |
	//
	// Classic European:
	// | Schnitzel        | Germany | 5          | 1          | 5          |
	// | Pizza            | Italy   | 4          | 100        | 2          |
	// | Spaghetti        | Italy   | 3          | 4          | 1          |
	// | Quiche           | France  | 1          | 10         | 1          |
	//
	// Exotic:
	// | Fried Rice       | Asia    | 3          | 50         | 1          |
	// | Chicken Jalfrezi | India   | 3          | 3          | 4          |
	// +------------------+---------+------------+------------+------------+
	//
	//         Note: 72% of these statistics are probably 86% inaccurate,
	//               or just completely made up :-)

}
