package psv_test

import (
	"fmt"

	"codeberg.org/japh/psv"
)

// ExampleTable_Encode_generate_table_from_table extends on ExampleTable_Encode_generate_table_from_data
// by using a PSV table for hard-coded data.
//
// See also ExampleTable_Encode_generate_table_from_data (generate_table_from_data_test.go)
func ExampleTable_Encode_generate_table_from_table() {

	yumminess, _ := psv.TableFromString(`


        +------------------+---------+------------+------------+------------+
        | name             | region  | popularity | variations | difficulty |
        | ================ | ======= | ========== | ========== | ========== |

Classic European:
        | Schnitzel        | Germany | 5          | 1          | 5          |
        | Pizza            | Italy   | 4          | 100        | 2          |
        | Spaghetti        | Italy   | 3          | 4          | 1          |
        | Quiche           | France  | 1          | 10         | 1          |

    Exotic:
        | Fried Rice       | Asia    | 3          | 50         | 1          |
        | Chicken Jalfrezi | India   | 3          | 3          | 4          |
        +------------------+---------+------------+------------+------------+

        Note: 72% of these statistics are probably 86% inaccurate,
              or just completely made up :-)


        `)

	// remove all indentation
	yumminess.SetIndent("0")

	fmt.Print(yumminess.Encode())

	// output:
	//
	// +------------------+---------+------------+------------+------------+
	// | name             | region  | popularity | variations | difficulty |
	// | ================ | ======= | ========== | ========== | ========== |
	//
	// Classic European:
	// | Schnitzel        | Germany | 5          | 1          | 5          |
	// | Pizza            | Italy   | 4          | 100        | 2          |
	// | Spaghetti        | Italy   | 3          | 4          | 1          |
	// | Quiche           | France  | 1          | 10         | 1          |
	//
	//     Exotic:
	// | Fried Rice       | Asia    | 3          | 50         | 1          |
	// | Chicken Jalfrezi | India   | 3          | 3          | 4          |
	// +------------------+---------+------------+------------+------------+
	//
	//         Note: 72% of these statistics are probably 86% inaccurate,
	//               or just completely made up :-)

}
