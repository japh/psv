package psv_test

import (
	"fmt"

	"codeberg.org/japh/psv"
)

func ExampleTable_preserve_non_data_lines() {

	tbl := psv.NewTable()
	tbl.Data = [][]string{
		{`stars`},
		{`1`},
		{`2`},
		{`3`},
		{`4`},
		{`5`},
	}

	tbl.TextLines = []*psv.TextLine{
		{Line: 1, Text: "Your personal star chart:"},
		{Line: 2},
		{Line: 4},
		{Line: 5, Text: "terrible! boo! take it away!"},
		{Line: 7},
		{Line: 8, Text: "average"},
		{Line: 12},
		{Line: 13, Text: "fantastic"},
		// there is no data and no text for line 13, so it will be blank
		{Line: 16, Text: "no more than 5 stars are available"},
	}

	// Note: non-data should be indented to match table data
	tbl.SetIndent(`>>>>`)

	fmt.Println(tbl.Encode())

	// output:
	//
	// Your personal star chart:
	//
	// >>>>| stars |
	//
	// terrible! boo! take it away!
	// >>>>| 1     |
	//
	// average
	// >>>>| 2     |
	// >>>>| 3     |
	// >>>>| 4     |
	//
	// fantastic
	// >>>>| 5     |
	//
	// no more than 5 stars are available
}
