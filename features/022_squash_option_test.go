package psv_test

import (
	"bufio"
	"fmt"
	"strings"

	"codeberg.org/japh/psv"
)

func ExampleOptions_Squash_only_squash_lines_within_table_boundaries() {

	tbl := psv.NewTable()
	tbl.DecodeString(`


                    +-
                    |||one|two||three|||
                    ||x|y|||||z|||

empty data rows are also squashed
                    |  |
                    |  |  |
                    |
                    ||
                    |    ||||||



squash multiple empty rows and columns



                    +-



`)

	tbl.Squash = true
	tbl.SetIndent(`>>>>`)

	output := tbl.Encode()
	r := strings.NewReader(output)
	s := bufio.NewScanner(r)
	l := 0
	for s.Scan() {
		l++
		fmt.Printf("%2d: %s.\n", l, s.Text())
	}

	// output:
	//
	//  1: .
	//  2: .
	//  3: .
	//  4: >>>>+---+-----+-----+-------+---+.
	//  5: >>>>|   | one | two | three |   |.
	//  6: >>>>| x | y   |     |       | z |.
	//  7: .
	//  8: empty data rows are also squashed.
	//  9: >>>>|   |     |     |       |   |.
	// 10: .
	// 11: squash multiple empty rows and columns.
	// 12: .
	// 13: >>>>+---+-----+-----+-------+---+.
	// 14: .
	// 15: .
	// 16: .
	//

}
