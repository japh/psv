package psv_test

import (
	"bufio"
	"fmt"
	"strings"

	"codeberg.org/japh/psv"
)

func ExampleOptions_preserve_or_squash_blank_lines() {

	tbl := psv.NewTable()
	tbl.Data = [][]string{
		{`number`},
		{`1`},
		{`2`},
		{`3`},
		{`4`},
		{`5`},
	}

	tbl.TextLines = []*psv.TextLine{
		{Line: 3},
		{Line: 4},
		{Line: 5},
		{Line: 9},
	}

	unsquashed := tbl.Encode()

	tbl.Squash = true
	squashed := tbl.Encode()

	fmt.Println("Unsquashed Table")
	r := strings.NewReader(unsquashed)
	s := bufio.NewScanner(r)
	l := 0
	for s.Scan() {
		l++
		fmt.Printf("%2d: %s.\n", l, s.Text())
	}
	fmt.Printf("unsquashed: %d lines\n", strings.Count(unsquashed, "\n"))

	fmt.Println()
	fmt.Println("Squashed Table")
	r = strings.NewReader(squashed)
	s = bufio.NewScanner(r)
	l = 0
	for s.Scan() {
		l++
		fmt.Printf("%2d: %s.\n", l, s.Text())
	}
	fmt.Printf("squashed:   %d lines\n", strings.Count(squashed, "\n"))

	// output:
	// Unsquashed Table
	//  1: | number |.
	//  2: | 1      |.
	//  3: .
	//  4: .
	//  5: .
	//  6: | 2      |.
	//  7: | 3      |.
	//  8: | 4      |.
	//  9: .
	// 10: | 5      |.
	// unsquashed: 10 lines
	//
	// Squashed Table
	//  1: | number |.
	//  2: | 1      |.
	//  3: .
	//  4: | 2      |.
	//  5: | 3      |.
	//  6: | 4      |.
	//  7: .
	//  8: | 5      |.
	// squashed:   8 lines

}
