package psv_test

import (
	"fmt"

	"codeberg.org/japh/psv"
)

func ExampleRuler_generate_from_format() {

	tbl := psv.NewTable()
	tbl.Data = [][]string{
		{``, `one`, `two`, `three`, `forty-two`},
	}

	// Trick: text lines without line numbers are simply added to the previous
	// block of decorative text (or inserted as the first line)
	tbl.TextLines = []*psv.TextLine{
		{Ruler: `-`, Line: 2}, // => format: ---- ruler: ---------------
		{Ruler: `=`},          // => format: ==== ruler: ===============
		{Ruler: `=-`},         // => format: =--= ruler: =----=--------=
		{Ruler: `=--`},        // => format: =--= ruler: =----=--------=
		{Ruler: `=---`},       // => format: =--- ruler: =-------------=
		{Ruler: `+-`},         // => format: +--+ ruler: +----+--------+
		{Ruler: `|-`},         // => format: |--| ruler: |----|--------|
		{Ruler: `|--`},        // => format: |--| ruler: |----|--------|
		{Ruler: `|--+`},       // => format: |--+ ruler: |----+--------|
		{Ruler: `|=-|`},       // => format: |=-| ruler: |=--=|=------=|
		{Ruler: `|=-+`},       // => format: |=-+ ruler: |=--=+=------=|
		{Ruler: `|==+`},       // => format: |==+ ruler: |====+========|
		{Ruler: `|===`},       // => format: |=== ruler: |=============|
		{Ruler: `| -`},        // => format: | -| ruler: | -- | ------ |
		{Ruler: `| =`},        // => format: | =| ruler: | == | ====== |
		{Ruler: `| ==`},       // => format: | == ruler: | == = ====== |
		{Ruler: `| -+`},       // => format: | -+ ruler: | -- + ------ |
	}

	fmt.Println(tbl.Encode())

	// output:
	//
	// |  | one | two | three | forty-two |
	// ------------------------------------
	// ====================================
	// =--=-----=-----=-------=-----------=
	// =--=-----=-----=-------=-----------=
	// =----------------------------------=
	// +--+-----+-----+-------+-----------+
	// |--|-----|-----|-------|-----------|
	// |--|-----|-----|-------|-----------|
	// |--+-----+-----+-------+-----------|
	// |==|=---=|=---=|=-----=|=---------=|
	// |==+=---=+=---=+=-----=+=---------=|
	// |==+=====+=====+=======+===========|
	// |==================================|
	// |  | --- | --- | ----- | --------- |
	// |  | === | === | ===== | ========= |
	// |  = === = === = ===== = ========= |
	// |  + --- + --- + ----- + --------- |
}
