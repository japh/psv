package psv_test

import (
	"fmt"

	"codeberg.org/japh/psv"
)

func ExampleTable_Encode_align_data_in_columns() {

	tbl := psv.NewTable()
	tbl.Data = [][]string{
		{`short`, `long`},
		{`1`, `this is the first data row`},
		{`2`, `this is the second data row`},
	}

	fmt.Println(tbl.Encode())

	// output:
	//
	// | short | long                        |
	// | 1     | this is the first data row  |
	// | 2     | this is the second data row |
}
