package psv_test

import (
	"fmt"

	"codeberg.org/japh/psv"
)

func ExampleOptions_parse_non_whitespace_indents() {

	// Note: "go test" fails examples when trailing whitespace is printed :-(
	text := `
            Some tables might be embedded in code comments
            (with odd indenting), like this:

            //  // Some of favourite things:
            //  //
            //  //      +-
            //  // |thing|feeling
            //  //+-
            //  //           |coffee|mmmmmmm
            //  //   |spring mornings|cheerful
            //  //      |fresh snow|powwwddddeeeerrr!
            //  //      +-
            //

            | kittens

            "psv -i //" should be enough to handle this :-)`

	indents := []string{``, `//`, `// `, ` //`, ` // `}

	fmt.Println("Original Text:")
	fmt.Println(text)

	for _, indent := range indents {
		tbl := psv.NewTable()
		// tbl.SetIndent(indent)
		tbl.DecodeString(text)

		fmt.Println("----")
		fmt.Printf("psv -i %q\n", indent)
		fmt.Printf("Indenter: %s\n", tbl.Indenter.String())
		fmt.Println("Formatted Text:")
		fmt.Print(tbl.Encode())
	}

	// output:
	//
	// Original Text:
	//
	//             Some tables might be embedded in code comments
	//             (with odd indenting), like this:
	//
	//             //  // Some of favourite things:
	//             //  //
	//             //  //      +-
	//             //  // |thing|feeling
	//             //  //+-
	//             //  //           |coffee|mmmmmmm
	//             //  //   |spring mornings|cheerful
	//             //  //      |fresh snow|powwwddddeeeerrr!
	//             //  //      +-
	//             //
	//
	//             | kittens
	//
	//             "psv -i //" should be enough to handle this :-)
	// ----
	// psv -i ""
	// Indenter: "            " (finalised)
	// Formatted Text:
	//
	//             Some tables might be embedded in code comments
	//             (with odd indenting), like this:
	//
	//             //  // Some of favourite things:
	//             //  //
	//             //  //      +-
	//             //  // |thing|feeling
	//             //  //+-
	//             //  //           |coffee|mmmmmmm
	//             //  //   |spring mornings|cheerful
	//             //  //      |fresh snow|powwwddddeeeerrr!
	//             //  //      +-
	//             //
	//
	//             | kittens |
	//
	//             "psv -i //" should be enough to handle this :-)
	// ----
	// psv -i "//"
	// Indenter: "            " (finalised)
	// Formatted Text:
	//
	//             Some tables might be embedded in code comments
	//             (with odd indenting), like this:
	//
	//             //  // Some of favourite things:
	//             //  //
	//             //  //      +-
	//             //  // |thing|feeling
	//             //  //+-
	//             //  //           |coffee|mmmmmmm
	//             //  //   |spring mornings|cheerful
	//             //  //      |fresh snow|powwwddddeeeerrr!
	//             //  //      +-
	//             //
	//
	//             | kittens |
	//
	//             "psv -i //" should be enough to handle this :-)
	// ----
	// psv -i "// "
	// Indenter: "            " (finalised)
	// Formatted Text:
	//
	//             Some tables might be embedded in code comments
	//             (with odd indenting), like this:
	//
	//             //  // Some of favourite things:
	//             //  //
	//             //  //      +-
	//             //  // |thing|feeling
	//             //  //+-
	//             //  //           |coffee|mmmmmmm
	//             //  //   |spring mornings|cheerful
	//             //  //      |fresh snow|powwwddddeeeerrr!
	//             //  //      +-
	//             //
	//
	//             | kittens |
	//
	//             "psv -i //" should be enough to handle this :-)
	// ----
	// psv -i " //"
	// Indenter: "            " (finalised)
	// Formatted Text:
	//
	//             Some tables might be embedded in code comments
	//             (with odd indenting), like this:
	//
	//             //  // Some of favourite things:
	//             //  //
	//             //  //      +-
	//             //  // |thing|feeling
	//             //  //+-
	//             //  //           |coffee|mmmmmmm
	//             //  //   |spring mornings|cheerful
	//             //  //      |fresh snow|powwwddddeeeerrr!
	//             //  //      +-
	//             //
	//
	//             | kittens |
	//
	//             "psv -i //" should be enough to handle this :-)
	// ----
	// psv -i " // "
	// Indenter: "            " (finalised)
	// Formatted Text:
	//
	//             Some tables might be embedded in code comments
	//             (with odd indenting), like this:
	//
	//             //  // Some of favourite things:
	//             //  //
	//             //  //      +-
	//             //  // |thing|feeling
	//             //  //+-
	//             //  //           |coffee|mmmmmmm
	//             //  //   |spring mornings|cheerful
	//             //  //      |fresh snow|powwwddddeeeerrr!
	//             //  //      +-
	//             //
	//
	//             | kittens |
	//
	//             "psv -i //" should be enough to handle this :-)

}
