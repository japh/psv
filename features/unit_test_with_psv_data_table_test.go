package psv_test

import (
	"fmt"
	"strconv"
	"strings"

	"codeberg.org/japh/psv"
)

func Example_test_function_with_psv_data() {

	type testCase struct {
		expr    string
		values  map[string]int
		result  int
		comment string
	}

	// Tip: command to reformat with psv in vim: va(!psv
	//
	testCases, _ := psv.TableFromString(`

	    | expression | values      | result | comment                          |
	    | ---------- | ----------- | ------ | -------------------------------- |
	    | a          | a:6         | 6      | an expressiong without operators |
	    :            :             :        :                                  :

	    Addition:
	    | a + b      | a:6 b:3     | 9      |                                  |
	    :            :             :        :                                  :

	    Subtraction:
	    | a - b      | a:6 b:3     | 3      |                                  |
	    :            :             :        :                                  :

	    Multiplication:
	    | a * b      | a:6 b:3     | 18     |                                  |
	    :            :             :        :                                  :

	    Division:
	    | a / b      | a:6 b:3     | 2      |                                  |
	    :            :             :        :                                  :

	    Precedence:
	    | a + b * c  | a:6 b:3 c:2 | 12     | first (b * c) then a + 6         |
	    :            :             :        :                                  :

	    `)

	// Data[1:] because the 1st row contains columnn headers and the second row
	// is a ruler (considered 'decorative text') and is not included in the data
	for _, tRow := range testCases.Data[1:] {

		// convert strings into our structure
		tc := testCase{}
		tc.expr = tRow[0]
		tc.values = map[string]int{}
		for _, v := range strings.Fields(tRow[1]) {
			s := strings.Split(v, ":")
			tc.values[s[0]], _ = strconv.Atoi(s[1])
		}
		tc.result, _ = strconv.Atoi(tRow[2])
		tc.comment = tRow[3]

		// same as ExampleTable_TypicalTestFunction()...
		if tc.comment != "" {
			fmt.Printf("// %s\n", tc.comment)
		}
		fmt.Printf("%q %+v => %d\n", tc.expr, tc.values, tc.result)

		// ... actually do some hard testing
	}

	// output:
	//
	// // an expressiong without operators
	// "a" map[a:6] => 6
	// "a + b" map[a:6 b:3] => 9
	// "a - b" map[a:6 b:3] => 3
	// "a * b" map[a:6 b:3] => 18
	// "a / b" map[a:6 b:3] => 2
	// // first (b * c) then a + 6
	// "a + b * c" map[a:6 b:3 c:2] => 12

}
