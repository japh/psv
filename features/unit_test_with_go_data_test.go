package psv_test

import (
	"fmt"
)

func Example_test_function_with_go_data() {

	type testCase struct {
		expr    string
		values  map[string]int
		result  int
		comment string
	}

	testCases := []testCase{
		{"a", map[string]int{"a": 6}, 6, "an expressiong without operators"},
		{"a + b", map[string]int{"a": 6, "b": 3}, 9, ""},
		{"a - b", map[string]int{"a": 6, "b": 3}, 3, ""},
		{"a * b", map[string]int{"a": 6, "b": 3}, 18, ""},
		{"a / b", map[string]int{"a": 6, "b": 3}, 2, ""},
		{"a + b * c", map[string]int{"a": 6, "b": 3, "c": 2}, 12, "first (b * c) then a + 6"},
	}

	for _, tc := range testCases {
		if tc.comment != "" {
			fmt.Printf("// %s\n", tc.comment)
		}
		fmt.Printf("%q %+v => %d\n", tc.expr, tc.values, tc.result)
		// ... actually do some hard testing
	}

	// output:
	//
	// // an expressiong without operators
	// "a" map[a:6] => 6
	// "a + b" map[a:6 b:3] => 9
	// "a - b" map[a:6 b:3] => 3
	// "a * b" map[a:6 b:3] => 18
	// "a / b" map[a:6 b:3] => 2
	// // first (b * c) then a + 6
	// "a + b * c" map[a:6 b:3 c:2] => 12

}
