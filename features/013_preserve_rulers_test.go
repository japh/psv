package psv_test

import (
	"fmt"

	"codeberg.org/japh/psv"
)

func ExampleRuler_preserve_rulers_or_fallback_to_text() {

	// Note: within psv, rulers are limited to lines containing only the
	//       characters [|:*=+-] and optional whitespace.
	//       The patterns used here are for demostrational purposes only.
	testCases := []string{
		// empty row - should never happen
		"",
		// 1..4 rune formats: [border, pad, line, sep]
		"=",
		"|-",
		"| -",
		"| -+",
		// > 4 characters are assumed to be previously generated rulers
		// if they can be reduced to a 4-rune format, it will be used to
		// generate a new ruler.
		// If not, the original input must be returned, unchanged
		"| - |",     // 1 column
		"|   |",     // 1 column, padding and line are both ' '
		"|    |",    // 1 column, padding and line are both ' ', not enough room for a 2nd column
		"|     |",   // dubious: could be 1 column, or 2 columns with pad, line and sep all identical
		"|  :  |",   // 2 empty columns
		"| - : - |", // 2 columns, normal
		"|  : - |",  // 2 columns, first column has 0 width
		// bad formats
		"| -:+",  // too many unique characters, but gets confused with a possibly valid table
		"| -:+*", // too many unique characters
		"|  | |", // implausible format, always expect 2 pads between borders
	}

	// examples using "| -:" format
	tables := [][]int{
		{0},    // |  |         not useful (no data), but possible
		{1},    // | - |        minimal
		{0, 1}, // | : - |      empty first column
		{1, 3}, // | - : --- |  normal
	}

TEST_CASE:
	for _, input := range testCases {
		for _, cols := range tables {
			fmt.Println()
			fmt.Printf("format:   %q\n", input)
			fmt.Printf("columns:  %+v\n", cols)

			// create initial ruler from format
			// Note: err from NewRuler() is only a warning
			//       r1 is always valid and usable
			r1, err := psv.NewRuler(input)
			r1Row := r1.Encode(cols)
			fmt.Printf("r1:       %q\n", r1Row)

			if err != nil {
				fmt.Printf("r1 error: %s\n", err)
				continue TEST_CASE
			}

			// create 2nd ruler from output of first ruler
			r2, err := psv.NewRuler(r1Row)
			if err != nil {
				fmt.Printf("r2 error: %s\n", err)
				continue TEST_CASE
			}

			// r2Row should always be the same as r1Row!
			r2Row := r2.Encode(cols)
			fmt.Printf("r2:       %q\n", r2Row)
		}
	}

	// output:
	//
	// format:   ""
	// columns:  [0]
	// r1:       "----"
	// r2:       "----"
	//
	// format:   ""
	// columns:  [1]
	// r1:       "-----"
	// r2:       "-----"
	//
	// format:   ""
	// columns:  [0 1]
	// r1:       "--------"
	// r2:       "--------"
	//
	// format:   ""
	// columns:  [1 3]
	// r1:       "-----------"
	// r2:       "-----------"
	//
	// format:   "="
	// columns:  [0]
	// r1:       "===="
	// r2:       "===="
	//
	// format:   "="
	// columns:  [1]
	// r1:       "====="
	// r2:       "====="
	//
	// format:   "="
	// columns:  [0 1]
	// r1:       "========"
	// r2:       "========"
	//
	// format:   "="
	// columns:  [1 3]
	// r1:       "==========="
	// r2:       "==========="
	//
	// format:   "|-"
	// columns:  [0]
	// r1:       "|--|"
	// r2:       "|--|"
	//
	// format:   "|-"
	// columns:  [1]
	// r1:       "|---|"
	// r2:       "|---|"
	//
	// format:   "|-"
	// columns:  [0 1]
	// r1:       "|--|---|"
	// r2:       "|--|---|"
	//
	// format:   "|-"
	// columns:  [1 3]
	// r1:       "|---|-----|"
	// r2:       "|---|-----|"
	//
	// format:   "| -"
	// columns:  [0]
	// r1:       "|  |"
	// r2:       "|  |"
	//
	// format:   "| -"
	// columns:  [1]
	// r1:       "| - |"
	// r2:       "| - |"
	//
	// format:   "| -"
	// columns:  [0 1]
	// r1:       "|  | - |"
	// r2:       "|  | - |"
	//
	// format:   "| -"
	// columns:  [1 3]
	// r1:       "| - | --- |"
	// r2:       "| - | --- |"
	//
	// format:   "| -+"
	// columns:  [0]
	// r1:       "|  |"
	// r2:       "|  |"
	//
	// format:   "| -+"
	// columns:  [1]
	// r1:       "| - |"
	// r2:       "| - |"
	//
	// format:   "| -+"
	// columns:  [0 1]
	// r1:       "|  + - |"
	// r2:       "|  + - |"
	//
	// format:   "| -+"
	// columns:  [1 3]
	// r1:       "| - + --- |"
	// r2:       "| - + --- |"
	//
	// format:   "| - |"
	// columns:  [0]
	// r1:       "|  |"
	// r2:       "|  |"
	//
	// format:   "| - |"
	// columns:  [1]
	// r1:       "| - |"
	// r2:       "| - |"
	//
	// format:   "| - |"
	// columns:  [0 1]
	// r1:       "|  | - |"
	// r2:       "|  | - |"
	//
	// format:   "| - |"
	// columns:  [1 3]
	// r1:       "| - | --- |"
	// r2:       "| - | --- |"
	//
	// format:   "|   |"
	// columns:  [0]
	// r1:       "|  |"
	// r2:       "|  |"
	//
	// format:   "|   |"
	// columns:  [1]
	// r1:       "|   |"
	// r2:       "|   |"
	//
	// format:   "|   |"
	// columns:  [0 1]
	// r1:       "|      |"
	// r2:       "|      |"
	//
	// format:   "|   |"
	// columns:  [1 3]
	// r1:       "|         |"
	// r2:       "|         |"
	//
	// format:   "|    |"
	// columns:  [0]
	// r1:       "|  |"
	// r2:       "|  |"
	//
	// format:   "|    |"
	// columns:  [1]
	// r1:       "|   |"
	// r2:       "|   |"
	//
	// format:   "|    |"
	// columns:  [0 1]
	// r1:       "|      |"
	// r2:       "|      |"
	//
	// format:   "|    |"
	// columns:  [1 3]
	// r1:       "|         |"
	// r2:       "|         |"
	//
	// format:   "|     |"
	// columns:  [0]
	// r1:       "|  |"
	// r2:       "|  |"
	//
	// format:   "|     |"
	// columns:  [1]
	// r1:       "|   |"
	// r2:       "|   |"
	//
	// format:   "|     |"
	// columns:  [0 1]
	// r1:       "|      |"
	// r2:       "|      |"
	//
	// format:   "|     |"
	// columns:  [1 3]
	// r1:       "|         |"
	// r2:       "|         |"
	//
	// format:   "|  :  |"
	// columns:  [0]
	// r1:       "|  |"
	// r2:       "|  |"
	//
	// format:   "|  :  |"
	// columns:  [1]
	// r1:       "|   |"
	// r2:       "|   |"
	//
	// format:   "|  :  |"
	// columns:  [0 1]
	// r1:       "|  :   |"
	// r2:       "|  :   |"
	//
	// format:   "|  :  |"
	// columns:  [1 3]
	// r1:       "|   :     |"
	// r2:       "|   :     |"
	//
	// format:   "| - : - |"
	// columns:  [0]
	// r1:       "|  |"
	// r2:       "|  |"
	//
	// format:   "| - : - |"
	// columns:  [1]
	// r1:       "| - |"
	// r2:       "| - |"
	//
	// format:   "| - : - |"
	// columns:  [0 1]
	// r1:       "|  : - |"
	// r2:       "|  : - |"
	//
	// format:   "| - : - |"
	// columns:  [1 3]
	// r1:       "| - : --- |"
	// r2:       "| - : --- |"
	//
	// format:   "|  : - |"
	// columns:  [0]
	// r1:       "|  |"
	// r2:       "|  |"
	//
	// format:   "|  : - |"
	// columns:  [1]
	// r1:       "| - |"
	// r2:       "| - |"
	//
	// format:   "|  : - |"
	// columns:  [0 1]
	// r1:       "|  : - |"
	// r2:       "|  : - |"
	//
	// format:   "|  : - |"
	// columns:  [1 3]
	// r1:       "| - : --- |"
	// r2:       "| - : --- |"
	//
	// format:   "| -:+"
	// columns:  [0]
	// r1:       "| -:+"
	// r1 error: "| -:+" has mismatching format characters
	//
	// format:   "| -:+*"
	// columns:  [0]
	// r1:       "| -:+*"
	// r1 error: "| -:+*" has too many unique characters to form a ruler (max 4)
	//
	// format:   "|  | |"
	// columns:  [0]
	// r1:       "|  | |"
	// r1 error: "|  | |" does not have plausibly spaced columns
}
