# BDD Feature Descriptions

This directory _should_ contain a `.feature` file for each functional feature
provided by PSV.

Sadly the BDD tooling for golang causes _me_ too much pain to use currently,
perhaps in the future I will overcome this hurdle too.

## Strategy

BDD (_Behaviour Driven Design_) is closely related to TDD (_Test Driven
Design_), in that the development process is roughly...

1. write a user story as scenarios in one or more gherkin feature files
2. step definitions should be written / updated to match the new requirements
3. the new / changed features should cause the test suite to fail
4. the code is modified to fulfill the new requirements
5. all tests should pass

One side effect of this process is that a project should never have many
un-implemented features. Releases are also only possible once _all_ tests pass.

In order to enable some kind of future-proofing / planing ability, in this
project, features are created in advance, but tags are used to filter which
features / scenarios are to be tested, and which should be skipped.

### Tags

| Tag        | When Tested                     | Description                                   |
| ---------- | ------------------------------- | --------------------------------------------- |
| `@backlog` | never                           | a new scenario which has not been implemented |
| `@wip`     | `make test` and `make wip-test` | a scenario currently being implemented        |
| `<none>`   | `make test` only                | all scenarios which are expected to pass      |

Non-development builds are expected to depend on `make test` passing, thus
forcing all quality checks to pass prior to deployment.
