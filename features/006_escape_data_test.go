//go:build ignore

package psv_test

import (
	"fmt"

	"codeberg.org/japh/psv"
)

func ExampleTable_Encode_escape_data() {

	tbl := psv.NewTable()
	// tbl.Quotes = `+` // use '+' for quoting instead of ", ', etc.
	// tbl.Quotes = `"` // use '+' for quoting instead of ", ', etc.
	// tbl.Quotes = `[` // use '+' for quoting instead of ", ', etc.
	tbl.TextLines = []*psv.TextLines{{Line: 2, Ruler: `| -`}}
	tbl.Data = [][]string{
		{`data`, `comment`},
		{``, `empty data - no special formatting`},
		{` `, `space`},
		{"\t", `tab`},
		{"\n", `newline (must be converted to \n)`},
		{`|`, `pipe`},
		{`"`, `double-quote`},
		{`'`, `single-quote`},
		{"`", `back-quote`},
		{` <`, `leading space`},
		{`> `, `trailing space`},
		{`> <`, `embedded space`},
		{` <> `, `surrounding space`},
		{` + `, `escape quoted quotes if needed`},
		{`:`, `no quoting needed`},
		{`;`, `no quoting needed`},
	}

	fmt.Println(tbl.Encode())

	// output:
	// | data   | comment                            |
	// |        | empty data - no special formatting |
	// | + +    | space                              |
	// | \t     | tab                                |
	// | \n     | newline (must be converted to \n)  |
	// | \|     | pipe                               |
	// | \"     | double-quote                       |
	// | \'     | single-quote                       |
	// | \`     | back-quote                         |
	// | > <    | embedded space                     |
	// | + <+   | leading space                      |
	// | +> +   | trailing space                     |
	// | + <> + | surrounding space                  |
}
