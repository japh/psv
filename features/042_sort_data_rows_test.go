package psv_test

import (
	"fmt"
	"strings"
	"testing"

	"codeberg.org/japh/is"
	"codeberg.org/japh/psv"
)

func TestEmptyTableNoSections(t *testing.T) {
	want := is.New(t)

	// Given
	tbl := psv.NewTable()

	// When
	s := tbl.Sections()

	// Then
	want.Equal(len(s), 0, "an empty table has no sections")
}

func TestOneRowTable(t *testing.T) {
	want := is.New(t)

	// Given a table with 1 row of data
	tbl := psv.New()
	tbl.AppendDataRow([]string{"one", "row", "only"})

	// When the list of sections is retrieved
	s := tbl.Sections()

	// Then
	want.Equal(len(s), 1, "a table with 1 row has a single section")
}

func TestTwoRowTable(t *testing.T) {
	want := is.New(t)

	// Given a table with two consecutive rows of data
	tbl := psv.New()
	tbl.AppendDataRow([]string{"first", "row", "of", "data"})
	tbl.AppendDataRow([]string{"second", "row", "of", "data"})

	// When the list of sections is retrieved
	s := tbl.Sections()

	// Then
	want.Equal(len(s), 1, "there should be 1 section")
	want.Equal(s[0].ColumnNames(), []string(nil), "and the column names should be nil")

	start, end := s[0].RowSpan()
	want.Equal(start, 0, "and the section's range should start on row 0")
	want.Equal(end, 2, "and the section's range should end on row 2")
}

func TestThreeRowTable(t *testing.T) {
	want := is.New(t)

	// Given a table with three consecutive rows of data
	tbl := psv.New()
	tbl.AppendDataRow([]string{"first", "row", "of", "data"})
	tbl.AppendDataRow([]string{"second", "row", "of", "data"})
	tbl.AppendDataRow([]string{"third", "row", "of", "data"})

	// When the list of sections is retrieved
	s := tbl.Sections()

	// Then
	want.Equal(len(s), 1, "there should be 1 section")
	want.Equal(s[0].ColumnNames(), []string(nil), "and the column names should be nil")

	start, end := s[0].RowSpan()
	want.Equal(start, 0, "and the section's range should start on row 0")
	want.Equal(end, 3, "and the section's range should end on row 3")
}

func TestDataRulerData(t *testing.T) {
	want := is.New(t)

	// Given a table with a data, ruler, data
	tbl := psv.New()
	tbl.AppendDataRow([]string{"first", "row", "of", "data"})
	tbl.AppendRuler("| -")
	tbl.AppendDataRow([]string{"second", "row", "of", "data"})

	// When the list of sections is retrieved
	s := tbl.Sections()

	// Then
	want.Equal(len(s), 2, "there should be 2 sections")
	want.Equal(s[0].ColumnNames(), []string{"first", "row", "of", "data"}, "and column names should be available")
	want.Equal(s[0].ColumnNames(), s[1].ColumnNames(), "and the data section's column names should be the same as the header section")
	want.Equal(s[0].ColumnNames(), tbl.ColumnNames(), "and the table's column names should be the same as the first header section")

	start, end := s[0].RowSpan()
	want.Equal(start, 0, "and the header's range should start on row 0")
	want.Equal(end, 1, "and the header's range should end on row 1")

	start, end = s[1].RowSpan()
	want.Equal(start, 1, "and the data section's range should start on row 1")
	want.Equal(end, 2, "and the data section's range should end on row 2")
}

func TestTableWithMultipleHeadersAndTextLines(t *testing.T) {
	want := is.New(t)

	// Given a table with a data, ruler, data
	tbl, _ := psv.TableFromString(`

                               +-------+-----+
                               | name  | age |
                               | ----- | --- |

                               toddlers:
                               | Andy  | 2   |

                               kids:
                               | Jane  | 16  |
                               | Steve | 12  |

                               grown ups:
                               | name  | age | sport  |
                               | ----- | --- | ------ |
                               | Bob   | 24  | tennis |
                               | Bea   | 24  | golf   |
                               | Alice | 42  | squash |

							   known bug:
							   the following, "single row of data
							   followed by a ruler" looks like a
							   header, but is actually just data.

							   That's not a problem though, since
							   1-line sections are never sorted anyway
							   and the header for preferences
							   re-defines the columns names anyway.

                               | Max   | 36  | golf   |
                               +-------+-----+--------+

                               Preferences:
                               +-------+--------+
                               | name  | color  |
                               | ----- | ------ |
                               | Steve | red    |
                               | Jane  | green  |
                               | Alice | orange |
                               | Bob   | blue   |
                               +-------+--------+

                               all done

                               `)

	expectedSections := []struct {
		start   int
		end     int
		name    string
		columns []string
	}{
		{0, 1, "", []string{"name", "age", ""}},
		{1, 2, "toddlers:", []string{"name", "age", ""}},
		{2, 4, "kids:", []string{"name", "age", ""}},
		{4, 5, "grown ups:", []string{"name", "age", "sport"}},
		{5, 8, "grown ups:", []string{"name", "age", "sport"}},
		{8, 9, "grown ups:", []string{"Max", "36", "golf"}}, // see comments in table above
		{9, 10, "Preferences:", []string{"name", "color", ""}},
		{10, 14, "Preferences:", []string{"name", "color", ""}},
	}

	// When the list of sections is retrieved
	gotSections := tbl.Sections()

	// Then
	ok := want.Equal(len(gotSections), len(expectedSections), "we should get the expected number of sections")
	if !ok {
		return
	}

	for i, e := range expectedSections {
		s := gotSections[i]
		start, end := s.RowSpan()

		t.Run(fmt.Sprintf("section #%d", i), func(t *testing.T) {
			want := is.New(t)
			want.Equal(s.Name(), e.name, "section name")
			want.Equal(s.ColumnNames(), e.columns, "column names")
			want.Equal(s.RowCount(), e.end-e.start, "section should have correct number of rows")
			want.Equal(start, e.start, "beginning of section's data row slice")
			want.Equal(end, e.end, "end of section's data row slice names")
		})
	}
}

func TestSortingWithinSections(t *testing.T) {
	want := is.New(t)

	// Given a table with a data, ruler, data
	tbl, _ := psv.TableFromString(`

                               +-------+-----+
                               | name  | age |
                               | ----- | --- |

                               toddlers:
                               | Andy  | 2   |

                               kids:
                               | Jane  | 16  |
                               | Steve | 12  |

                               grown ups:
                               | name  | age | sport  |
                               | ----- | --- | ------ |
                               | Bob   | 24  | tennis |
                               | Bea   | 24  | golf   |
                               | Alice | 42  | squash |

							   known bug:
							   the following, "single row of data
							   followed by a ruler" looks like a
							   header, but is actually just data.

							   That's not a problem though, since
							   1-line sections are never sorted anyway
							   and the header for preferences
							   re-defines the columns names anyway.

                               | Max   | 36  | golf   |
                               +-------+-----+--------+

                               Preferences:
                               +-------+--------+
                               | name  | color  |
                               | ----- | ------ |
                               | Steve | red    |
                               | Jane  | green  |
                               | Alice | orange |
                               | Bob   | blue   |
                               +-------+--------+

                               all done

                               `)

	ageNameData := [][]string{
		strings.Split("name|age|", "|"),
		//
		strings.Split("Andy|2|", "|"),
		//
		strings.Split("Jane|16|", "|"),  // not sorted - not in grown-ups or preferences sections
		strings.Split("Steve|12|", "|"), // not sorted - not in grown-ups or preferences sections
		//
		strings.Split("name|age|sport", "|"),
		//
		strings.Split("Bea|24|golf", "|"),   // swapped with Bob
		strings.Split("Bob|24|tennis", "|"), // swapped with Bea
		strings.Split("Alice|42|squash", "|"),
		//
		strings.Split("Max|36|golf", "|"),
		//
		strings.Split("name|color|", "|"),
		//
		strings.Split("Bob|blue|", "|"),     // preferences sorted by column #2 (color)
		strings.Split("Jane|green|", "|"),   // preferences sorted by column #2 (color)
		strings.Split("Alice|orange|", "|"), // preferences sorted by column #2 (color)
		strings.Split("Steve|red|", "|"),    // preferences sorted by column #2 (color)
	}

	sorter := tbl.NewSorter()
	sorter.InSections([]string{"grown ups", "8"})   // section #8: preferences
	sorter.ByColumns([]string{"age#", "2", "name"}) // column #2: age or color
	// sorter.InSections([]string{"none"}) // should match no sections
	// sorter.ByColumns([]string{"3"})                 // column #2: age or color
	sorter.Sort()

	for r, expectedRow := range ageNameData {
		want.Equal(tbl.Data[r], expectedRow, fmt.Sprintf("row %2d should be %v", r, expectedRow))
	}

}
