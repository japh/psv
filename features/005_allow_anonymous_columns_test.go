package psv_test

import (
	"fmt"

	"codeberg.org/japh/psv"
)

func ExampleTable_allow_anonymous_columns() {

	tbl := psv.NewTable()
	tbl.Data = [][]string{
		{`number`},
		{`1`},
		{`2`, `two is better`},
	}

	fmt.Println(tbl.Encode())

	// output:
	//
	// | number |               |
	// | 1      |               |
	// | 2      | two is better |
}
