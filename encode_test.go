package psv_test

import (
	"fmt"

	"codeberg.org/japh/psv"
)

func ExampleTable_Encode_encoding_decorative_text_lines() {

	tbl := psv.NewTable()

	tbl.SetIndent("....")
	tbl.TextLines = []*psv.TextLine{
		// column headers
		{Line: 2, Ruler: "+ ="},
		{Line: 3},
		{Line: 4, Text: "Empty rows are kept"},
		{Line: 5, Text: "Text lines appear between rows"},
		{Line: 7},
		{Line: 7, Text: "# Comments are always good"},
		{Line: 7, Text: "# repeating line numbers is not good, but works"},
		// data...
		{Line: 12},
		{Line: 12, Text: "leading and trailing spaces need extra quotes"},
		{Line: 15},
		{Line: 15, Text: "Rulers can be placed anywhere"},
		{Line: 15, Ruler: "+ -"},
		{Line: 20, Text: "and trailing text lines are no problem either"},
	}
	tbl.Data = [][]string{
		{"one", "foo", "two", "three"}, // first row are headers only
		{},                             // empty row
		{"the first", "", "the second", "the third"}, // full row
		{"more data"},                             // partial row
		{`" "`, `" x "`, `'"'`, `" x"`},           // handling of leading and trailing spaces
		{"lorem", "", "ipsum", "upsum", "oopsum"}, // extra column!
	}

	fmt.Print(tbl.Encode())

	// output:
	//
	// ....| one       | foo   | two        | three     |        |
	// ....+ ========= + ===== + ========== + ========= + ====== +
	//
	// Empty rows are kept
	// Text lines appear between rows
	// ....|           |       |            |           |        |
	//
	// # Comments are always good
	// # repeating line numbers is not good, but works
	// ....| the first |       | the second | the third |        |
	// ....| more data |       |            |           |        |
	//
	// leading and trailing spaces need extra quotes
	// ....| " "       | " x " | '"'        | " x"      |        |
	//
	// Rulers can be placed anywhere
	// ....+ --------- + ----- + ---------- + --------- + ------ +
	// ....| lorem     |       | ipsum      | upsum     | oopsum |
	//
	// and trailing text lines are no problem either
	//
}
