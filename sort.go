package psv

import (
	"fmt"
	"regexp"
	"sort"
	"strconv"

	"golang.org/x/text/collate"
	"golang.org/x/text/language"
)

const (
	lexographicOrderRune = '@' // text-only order, e.g. 10 < 2 (default)
	numericOrderRune     = '#' // numeric order, e.g. 2 < 10
	reverseOrderRune     = '~' // toggle between ascending and descending order
)

const (
	forwardSortDir = +1
	reverseSortDir = -1 // used to toggle between +1 and -1 via sortDir *= -1
)

type Sorter struct {
	table          *Table
	sectionFilters []*nameOrIndexFilter
	columnFilters  []*nameOrIndexFilter
	languageTag    language.Tag
	sortDir        int
	collatorRune   rune
}

type nameOrIndexFilter struct {
	pattern      *regexp.Regexp
	index        int
	sortDir      int
	collatorRune rune
}

func (tbl *Table) NewSorter() *Sorter {
	s := &Sorter{
		table:        tbl,
		sortDir:      forwardSortDir,
		collatorRune: numericOrderRune, // sort lexographically by default
		languageTag:  language.Und,     // default collation rules generic / 'undetermined'
	}

	return s
}

func (s *Sorter) Language(bcp47Tag string) {
	s.languageTag = language.Make(bcp47Tag)
}

func (s *Sorter) InSections(names []string) error {
	filters, err := s.newFilters(names)
	if err != nil {
		return err
	}
	s.sectionFilters = filters
	return nil
}

func (s *Sorter) ByColumns(names []string) error {
	filters, err := s.newFilters(names)
	if err != nil {
		return err
	}
	s.columnFilters = filters
	return nil
}

func (s *Sorter) newFilters(names []string) ([]*nameOrIndexFilter, error) {

	if len(names) == 0 {
		return nil, nil
	}

	filters := make([]*nameOrIndexFilter, 0, len(names))

FILTERS:
	for _, name := range names {
		filter := &nameOrIndexFilter{
			sortDir:      forwardSortDir,
			collatorRune: s.collatorRune,
		}

	CONTROL_PREFIXES:
		for len(name) > 0 {
			switch ctrl := name[0]; ctrl {
			case reverseOrderRune:
				name = name[1:]
				filter.sortDir *= reverseSortDir
				continue
			case lexographicOrderRune:
				name = name[1:]
				filter.collatorRune = lexographicOrderRune
				continue
			case numericOrderRune:
				name = name[1:]
				filter.collatorRune = numericOrderRune
				continue
			default:
				// finished stripping control flags
				break CONTROL_PREFIXES
			}
		}

		if len(name) == 0 {
			// not a column filter, but generic configuration
			s.collatorRune = filter.collatorRune
			s.sortDir *= filter.sortDir
			continue FILTERS
		}

		pattern, err := regexp.Compile(fmt.Sprintf("(?i:%s)", name))
		if err != nil {
			return nil, err
		}
		filter.pattern = pattern

		if index, err := strconv.Atoi(name); err == nil {
			filter.index = index
		}

		filters = append(filters, filter)
	}

	return filters, nil
}

func (s *Sorter) Sort() error {
	s.table.analyseTableStructure()

	// setup collators
	collators := map[rune]*collate.Collator{
		lexographicOrderRune: collate.New(s.languageTag),
		numericOrderRune:     collate.New(s.languageTag, collate.Numeric),
	}

	// find all sections to be sorted
	//	- skip sections with 1 row
	//	- skip sections if we have one or more name patterns and none of them match
	sectionCount := 0

	for index, section := range s.table.Sections() {
		if section.RowCount() <= 1 {
			continue
		}

		wanted, sectionFilter := s.wantNameOrIndex(section.Name(), index+1, s.sectionFilters)
		if !wanted {
			continue
		}

		// each section has a different slice of rows and may have a different
		// set of columns, in which case the sorting rules will also be
		// different
		start, end := section.RowSpan()
		ss := &sectionSorter{
			data: s.table.Data[start:end],
		}

		if sectionFilter != nil {
			ss.sortDir = sectionFilter.sortDir
			ss.collator = collators[sectionFilter.collatorRune]
		} else {
			ss.sortDir = s.sortDir
			ss.collator = collators[s.collatorRune]
		}

		for _, filter := range s.columnFilters {
			matched := false
			for index, name := range section.ColumnNames() {
				if wanted, filter := s.wantNameOrIndex(name, index+1, []*nameOrIndexFilter{filter}); wanted {
					// we know that filter cannot be nil here!
					matched = true
					ss.cmp = append(ss.cmp,
						&comparitor{
							index:    index,
							sortDir:  filter.sortDir,
							collator: collators[filter.collatorRune],
						})
				}
			}
			if !matched && filter.index != 0 {
				ss.cmp = append(ss.cmp,
					&comparitor{
						index:    filter.index - 1,
						sortDir:  filter.sortDir,
						collator: collators[filter.collatorRune],
					})
			}
		}

		sort.Stable(ss)
	}

	if sectionCount == 0 && len(s.sectionFilters) > 0 {
		return fmt.Errorf("no sections matched section filters")
	}

	return nil
}

type sectionSorter struct {
	sortDir  int
	collator *collate.Collator
	cmp      []*comparitor
	data     [][]string
}

type comparitor struct {
	index    int // section or column index (0-based index into table.Data rows)
	sortDir  int // forwardSortDir | reverseSortDir
	collator *collate.Collator
}

func (ss *sectionSorter) Len() int {
	return len(ss.data)
}

func (ss *sectionSorter) Swap(a, b int) {
	ss.data[a], ss.data[b] = ss.data[b], ss.data[a]
}

func (ss *sectionSorter) Less(a, b int) bool {
	cmp := ss.cmp
	m := len(cmp)

	// default sort - sort columns from left to right until we run out of columns
	if m == 0 {

		i := 0
		for i = range ss.data[a] {
			if i >= len(ss.data[b]) {
				// row b has less columns that row a
				// => a must be "greater or equal" than b, depending on sortDir
				return ss.sortDir < 0
			}

			aCell := ss.data[a][i]
			bCell := ss.data[b][i]
			diff := ss.collator.CompareString(aCell, bCell) * ss.sortDir
			switch {
			case diff < 0:
				return true
			case diff > 0:
				return false
			default:
				// case diff == 0 loop to next column
			}

			if i < len(ss.data[b])-1 {
				// row a has less columns that row b
				return ss.sortDir > 0
			}

			// both rows are equal
			// => always false
			return false
		}
	}

	// custom sort - sort columns in the order defined by the column filters
	for _, c := range cmp {

		aCell := ""
		bCell := ""

		if c.index < len(ss.data[a]) {
			aCell = ss.data[a][c.index]
		}
		if c.index < len(ss.data[b]) {
			bCell = ss.data[b][c.index]
		}

		diff := c.collator.CompareString(aCell, bCell) * c.sortDir * ss.sortDir

		switch {
		case diff < 0:
			return true
		case diff > 0:
			return false
		default:
			// case diff == 0 loop to next column
		}
	}

	// both rows are equal
	// => always false
	return false
}

// wantNameOrIndex determines if the provided name and/or index match a list of filters
//
// name may be empty
// index should be a 1-based integer indicating the position of this item within its structure
// (i.e. 1 + the array index of the section or column)
func (s *Sorter) wantNameOrIndex(name string, index int, filters []*nameOrIndexFilter) (bool, *nameOrIndexFilter) {
	// no filters? want everything
	if len(filters) == 0 {
		return true, nil
	}

	// do we want this name?
	if name != "" {
		for _, f := range filters {
			if f.pattern != nil && f.pattern.MatchString(name) {
				return true, f
			}
		}
	}

	for _, f := range filters {
		// warning: f.index is 1 based (f.index == 0: unset / ignore)
		if f.index > 0 && index == f.index {
			return true, f
		}
	}

	return false, nil
}
