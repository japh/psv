package psv

import (
	"fmt"
	"unicode/utf8"
)

type encoder struct {
	*Table
	buffer     Writer
	data       [][]string
	textQueue  *textQueue
	rows, cols int
	width      []int
	line       int
	firstRow   int
	lastRow    int
	squashLine int
	squashRow  int
	warnings   []warning
}

type warning struct {
	line int
	err  error
}

// Writer interface used to produce new PSV tables.
// Warning: WriteString will be called many times per line.
type Writer interface {
	WriteString(string) (int, error) // e.g. bytes.Builder or strings.Builder
}

// Encode converts the 2-Dimensional slice of string data in the table into a
// pretty, pipe-separated table as a single, printable string.
func (tbl *Table) Write(b Writer) {

	e := &encoder{
		Table:     tbl,
		buffer:    b,
		textQueue: &textQueue{tbl.TextLines},
		width:     []int{},
		warnings:  []warning{},
	}

	e.encode()
}

// AnalyseData was only created for testing (to allow access to the internal workings of encode()).
//
// Never call this - it provides nothing of use.
func (tbl *Table) AnalyseData() (rows, cols int, width []int) {
	e := &encoder{
		Table: tbl,
		width: []int{},
	}
	e.scanDataDimensions()
	e.copyData()
	e.scanColumnWidths()
	rows = e.rows
	cols = e.cols
	width = e.width
	return
}

// merge text lines and data rows into a single multi-line string
//  - setup a queue of decorative text lines to be inserted
//  - add text lines from the queue if their line number is <= the current line
//	  in the buffer
//  - otherwise, add a data row
//  - after the last data row, deplete the decorative text queue
func (e *encoder) encode() {

	// REFACTOR(Steve) 2022-08-24 extract sorting and table structure from Table
	e.Table.Sort()

	e.scanDataDimensions()
	e.copyData()
	// e.quoteData()    // NOT YET IMPLEMENTED
	e.scanColumnWidths()

	// text always points to the next decorative text line to be pulled
	// from the text queue
	text := e.textQueue.next()

	// write data and text lines
	e.line = 1
	for _, row := range e.data {
		for text != nil && text.Line <= e.line {
			e.encodeDecoration(text)
			text = e.textQueue.next()
		}
		e.encodeDataRow(row)
	}

	// add trailing decorative text lines
	for text != nil {
		for e.line < text.Line {
			e.encodeEmptyLine()
		}
		e.encodeDecoration(text)
		text = e.textQueue.next()
	}

	// add warnings
	if len(e.warnings) > 0 {
		e.encodeWarnings()
	}
}

func (e *encoder) scanDataDimensions() {
	cols := 0
	for _, row := range e.Table.Data {
		fields := len(row)
		if fields > cols {
			cols = fields
		}
	}
	e.rows = len(e.Table.Data)
	e.cols = cols

	if e.lastRow == 0 {
		line := 1
		for _, deco := range e.Table.TextLines {

			if e.firstRow == 0 {
				if line < deco.Line || deco.Ruler != "" {
					e.firstRow = line
					e.lastRow = line + e.rows
				} else {
					line++
					continue
				}
				// fall through to internal deco check
			}

			if deco.Line < e.lastRow {
				e.lastRow++
				continue
			}

			if deco.Line > e.lastRow && deco.Ruler != "" {
				e.lastRow = deco.Line
				continue
			}

		}
	}
}

// be nice: if we were given data by an application, create a duplicate of the
// table, but don't duplicate the table's contents!
// i.e.: e.data points to the original data, until we start formatting, then it
// will point to the formatted strings instead
func (e *encoder) copyData() {
	// Note: no row can have more than e.cols field - see scanDataDimensions()
	e.data = make([][]string, e.rows)
	for r, row := range e.Table.Data {
		e.data[r] = make([]string, e.cols)
		for c := range row {
			// really only a pointer-copy (strings are immutable, changing a
			// string (e.g. via quoting) will give us new pointers while
			// leaving the original table unchanged)
			e.data[r][c] = row[c]
		}
	}
}

// func (e *encoder) quoteData() {
// 	// fmt.Printf("quoting with preferred quotes: %s\n", e.Table.Quotes)
// 	type cellCoord struct{ r, c int }
// 	prefQuotes := strings.Split(e.Table.Quotes, "") // available quoting styles, in order of preference
// 	quoteRank := map[string]int{}                   // find the most appropriate set of quotes for this table
// 	// badQuotes := map[rune]struct{}{}                // quotes that need qoting
//
// 	closingQuote := map[string]string{
// 		`(`: `)`,
// 		`{`: `}`,
// 		`[`: `]`,
// 		`<`: `>`,
// 	}
//
// 	// find out how many cells contain each of our preferred (opening) quotes
// 	quotesRE := regexp.MustCompile(`[` + regexp.QuoteMeta(e.Table.Quotes) + `]`)
// 	// fmt.Printf("looking for cells that contain quotes: %v\n", quotesRE)
// 	for _, row := range e.data {
// 		for _, cell := range row {
// 			if matches := quotesRE.FindAllString(cell, -1); matches != nil {
// 				// fmt.Printf("found cell with quote chars [%d,%d] %q\n", r, c, cell)
// 				for _, matchedQuote := range matches {
// 					quoteRank[matchedQuote]++
// 				}
// 			}
// 		}
// 	}
//
// 	// find the preferred quoting character(s) which would require the least escaping
// 	sort.SliceStable(prefQuotes,
// 		func(a, b int) bool {
// 			qa, qb := prefQuotes[a], prefQuotes[b]
// 			ra, rb := quoteRank[qa], quoteRank[qb]
// 			return ra < rb
// 		})
//
// 	oq := prefQuotes[0]
// 	cq := closingQuote[oq]
// 	if cq == "" {
// 		cq = oq
// 	}
//
// 	// fmt.Printf("rank-sorted quotes: %+v\n", prefQuotes)
//
// 	// var oq string
// 	// for _, p := range prefQuotes {
// 	// 	if _, skip := quoteRank[p]; skip {
// 	// 		fmt.Printf("skipping quote %s\n", p)
// 	// 		continue
// 	// 	}
// 	// 	oq = p
// 	// 	break
// 	// }
// 	// cq, hasCloser := closingQuote[oq]
// 	// if !hasCloser {
// 	// 	cq = oq
// 	// }
//
// 	// fmt.Printf("preferred quotes: %s...%s\n", oq, cq)
//
// 	/*
//
// 		// badRE := regexp.MustCompile(`([|` + regexp.QuoteMeta(e.Table.Quotes) + `])|^[\s\pZ]+|[\s\pZ]+$`)
//
// 		escape := map[string]string{ // characters that need conversion
// 			"\n": `\n`,
// 			"\t": `\t`,
// 			oq:   `\` + oq,
// 			cq:   `\` + cq,
// 		}
// 		escStrs := make([]string, len(escape))
// 		for k := range escape {
// 			escStrs = append(escStrs, k)
// 		}
// 		escRE := regexp.MustCompile("[" + regexp.QuoteMeta(strings.Join(escStrs, "")) + "]")
//
// 		for _, cell := range quoteCells {
// 			bd := &e.data[cell.r][cell.c]
// 			fmt.Printf("unquoted [%d,%d] %q\n", cell.r, cell.c, *bd)
//
// 			*bd = escRE.ReplaceAllStringFunc(*bd, func(raw string) string { return escape[raw] })
// 			*bd = oq + *bd + cq
// 			fmt.Printf("quoted [%d,%d] %q (was %q)\n", cell.r, cell.c, e.data[cell.r][cell.c], e.Table.Data[cell.r][cell.c])
// 		}
//
// 	*/
// }

// determine the character width of each column in the table
func (e *encoder) scanColumnWidths() {
	width := []int{}
	for _, row := range e.data {
		for c := range row {
			w := utf8.RuneCountInString(row[c])
			if c >= len(width) {
				width = append(width, w)
				continue
			}
			if w > width[c] {
				width[c] = w
			}
		}
	}
	e.width = width
}

func (e *encoder) isOnTableRow() bool {
	return e.line >= e.firstRow &&
		e.line <= e.lastRow
}

func (e *encoder) isSquashLine() bool {
	return e.Table.Squash && e.line == e.squashLine
}

// encode a text line, ruler
func (e *encoder) encodeDecoration(text *TextLine) {
	switch {
	case text.Ruler != "":
		e.encodeRuler(text)
	case text.Text == "":
		e.encodeEmptyLine()
	default:
		e.buffer.WriteString(text.Text)
		e.buffer.WriteString("\n")
		e.line++
	}
}

func (e *encoder) encodeEmptyLine() {
	if e.isOnTableRow() {
		if !e.isSquashLine() {
			e.buffer.WriteString("\n")
		}
	} else {
		e.buffer.WriteString("\n")
	}
	e.line++
	e.squashLine = e.line
}

func (e *encoder) encodeTextLine(text *TextLine) {
	if e.isOnTableRow() {
		e.buffer.WriteString(e.Table.Indent())
	} else {
		e.buffer.WriteString(text.Indent)
	}
	e.buffer.WriteString(text.Text)
	e.buffer.WriteString("\n")
	e.line++
}

func (e *encoder) encodeRuler(text *TextLine) {
	ruler, err := NewRuler(text.Ruler)
	if err != nil {
		e.warnings = append(e.warnings, warning{e.line, err})
	}

	// squash empty columns if needed
	width := e.width
	if e.Table.Squash {
		width = []int{}
		for _, w := range e.width {
			if w > 0 {
				width = append(width, w)
			}
		}
	}

	e.buffer.WriteString(e.Table.Indent())
	e.buffer.WriteString(ruler.Encode(width))
	e.buffer.WriteString("\n")
	e.line++
}

func (e *encoder) encodeDataRow(row []string) {
	// squash empty rows
	if e.Table.Squash {
		isEmpty := true
		for _, c := range row {
			if c != "" {
				isEmpty = false
				break
			}
		}
		if isEmpty {
			squash := e.line == e.squashRow
			e.squashRow = e.line + 1
			if squash {
				e.line++
				return
			}
		}
	}

	e.buffer.WriteString(e.Table.Indent())
	e.buffer.WriteString("|")
	for c, w := range e.width {
		if e.Table.Squash && w == 0 {
			continue
		}
		if c >= len(row) {
			e.buffer.WriteString(fmt.Sprintf(" %-*s |", w, ""))
		} else {
			e.buffer.WriteString(fmt.Sprintf(" %-*s |", w, row[c]))
		}
	}
	e.buffer.WriteString("\n")
	e.line++
}

func (e *encoder) encodeWarnings() {
	e.buffer.WriteString("\n--\n")
	e.buffer.WriteString("Warnings:\n")
	lw := len(fmt.Sprintf("%d", e.line))
	for _, w := range e.warnings {
		e.buffer.WriteString(fmt.Sprintf("  line %*d: %s\n", lw, w.line, w.err))
	}
	e.buffer.WriteString("--\n\n")
}
