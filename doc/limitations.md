# Known Limitations

- only simple, 2-dimensional data can be represented
    - exception: short *lists-of-lists* or *lists-of-named-values* are possible, via a hack.
    - e.g.:

        ```
        | expression    | values      | result |
        | ------------- | ----------- | ------ |
        | a + b * c     | a:3 b:4 c:5 | 23     |
        | ( a + b ) * c | a:3 b:4 c:5 | 35     |
        ```

- the `|` character cannot be represented as part of the data
    - this is only true when using `psv` to re-format existing tables, or when
      reading data from a table electronically.
    - **future:** add quoting and escaping rules to allow *special characters* to be included

- **future:** *column re-ordering*
    - suggestion:
        - add a `psv -c <from>:<to>` option to allow moving a column to a different location.
        - `<from>` and `<to>` would be column numbers (1, 2, 3, ...)
        - `psv -c <from>:` could be used to delete columns
        - `psv -c :<to>` could be used to add columns
        - `psv -c 4+3:2` would move the columns 4, 5, 6 and 7 to column 2
          (equivalent to moving columns 2 and 3 to column 8, or `psv -c 2,3:8`)
        - `psv -c 4,7,8:` would delete the columns 4, 7 and 8
        - `psv -c :5+4` would create 4 (or 5?) columns, starting at column 5

- **future:** add support for left|center|right alignment
    - https://github.github.com/gfm/#tables-extension- defines tables as:
        - a header row, `|` separated
        - a delimeter row, consisting of only `|`, `-`, `:` and whitespace
        - each delimiter cell must be one or more `-`s, with optional `:`s to
          specify the desired alignment:
            - `-` or `:-` left alignment
            - `-:` right alignment
            - `:-:` centered
        - "The header row must match the delimiter row in the number of cells."
