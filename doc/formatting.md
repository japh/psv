# `psv` Table Formatting Rules

*TODO*

- [ ] explain round-trip formatting
- [ ] remove duplicate sections
- [ ] formatting tables (writing)
  - [ ] indentation applies to all lines between the first and last table rows
    - [ ] rows before/after the table are left unchanged
  - [ ] every *data* row begins and ends with a `|`
  - [ ] columns are separated by a `|` character only
  - [ ] the first row has no special meaning
  - [ ] rulers are stretched to fit the entire width of the table
  - [ ] rulers may be used before the first and after the last row of data
  - [ ] empty data rows, empty lines and empty columns are retained by default
    - [x] the `Squash` option (or `psv -squash`) may be used to *squash* empty columns and lines
- [ ] reading tables
  - [ ] ***any*** table produced by `psv` ***must*** be readable by `psv`
    - [ ] re-formatting a table which was previously formated by `psv` should have no effect!
      - [ ] the `psv` format is idempotent
    - [ ] this applies to all:
      - [ ] data rows
      - [ ] rulers
      - [ ] non-table lines within the table
    - [ ] text before the first table row or ruler should never be changed
    - [ ] text after the last table row or ruler should never be changed
    - [ ] *custom* indentation (such as comment characters) may required the
          use of an *indentation* option (`psv -i ...` or `psv.Table.Options.Indent`)
  - [ ] data row recognition:
  - [ ] ruler recognition
  - [ ] indentation parsing
    - [ ] default: whitespace only
    - [ ] custom:
      - [ ] whitespace only (including `""`)
      - [ ] `#` (no whitespace)
      - [ ] `<space>#` (explicit leading whitespace)
      - [ ] `#<space>`    (explicit trailing whitespace)
      - [ ] `#<space>#` (embedded spacing)
      - [ ] `<space>#<space>` (leading and trailing whitespace)

## Index

- [Basic Concept](#basic-concept)
- [Reading PSV Tables](#reading-psv-tables)
    - [Indentation](#indentation)
    - [Finding Data](#finding-data)
- [Writing PSV Tables](#writing-psv-tables)
    - [Data Rows](#data-rows)
    - [Squashing](#squashing)
    - [Rulers](#rulers)
    - [Additional Text](#additional-text)
- [Markdown Tables](#markdown-tables)

## Basic Concept

`psv` looks for a table of *pipe separated values* in any block of
text and is able to format tabular data using the rules defined here.

 Any table rows will be formatted so that all rows
have the same width and the columns align nicely.

This file describes the PSV format from two separate perspectives:

- [reading PSV tables](#reading-psv-tables)
- [Writing PSV tables](#writing-psv-tables)

The [`psv` command line tool](cli.md) combines both aspects, first
reading data from `STDIN` and then producing a formatted version of
the data to `STDOUT`.

## Reading PSV Tables

`psv` classifies each line of text as:

- a [**data row**](#data-rows)
  - any line beginning with `|`
- a [**ruler**](#rulers) (or *separator line*)
  - lines which contain *only* a selection of the characters `|`, `:`, `#`, `=`, `~`, `-`, `+` or whitespace
- or [**text**](#text)
  - any lines which are neither *data rows* nor *rulers*


- any data provided to `psv` is assumed to be in [utf-8](https://en.wikipedia.org/wiki/UTF-8) format
- the input data is processed line-by-line, with lines separated by
  newlines (`\n`) or carraige-return newline pairs (`\r\n`)
- any line which does not *look like* a table row is ignored

### Indentation

When reading a PSV table, leading whitespace (`^[\s\pZ]*`) is *always*
skipped before further processing.

The following lines are all equivalent:

```
|...|...|...
    |...|...|...
                |...|...|...
```


        - ***Note:*** leading and trailing whitespace is *always* removed from
        table rows (any line of text beginning with a `|` character) 

        - *by default* (when `-indent` is not used), the indentation of the ***first
            table row*** is used for all table rows

        - if `<indent>` consists of whitespace *only*, then the provided whitespace will
            be used to align all table rows to the same level of indentation

        - `-indent` `<number>`

            - the indentation of each table row will be set to `<number>` spaces

        - `-indent ""` or `-indent 0` may be used to remove all indentation

        - if `<indent>` contains non-whitespace characters (e.g. `-i //` or `-i "#"`):

            - the provided characters will be also be ignored when looking for table rows

            - the provided characters may be duplicated, with any amount of whitespace between them

            - if `<indent>` begins or ends with whitespace (e.g. `-i "# "`,
                `-i " //"` or `-i " -- "`), then the provided whitespace will
                be used when indenting the table rows

            - if `<indent>` begins or ends with a non-whitespace character, the
                respective leading and/or trailing indentation will be taken
                from the first table row

            - if `<indent>` has no leading or trailing whitespace, the exact
                indentation of the first row will be used for the entire table.

                e.g.: `psv -i #`
                ```
                    # #   # | data
                #|data
                ```
                would become
                ```
                    # #   # | data |
                    # #   # | data |
                ```

            - embedded whitespace (e.g. `-i "//  #"`) will always be retained and must match exactly

            - see the [indentation](formatting.md#indentation) section of the
                formatting rules for details and examples

        - otherwise, the indent string will be ignored from each text line
            during parsing and will be inserted before each table row while
            formatting. (see [formatting rules](formatting.md) for details)

#### Accessing PSV Data In Comments

`psv` will *not* find your table if is preceeded by comments, for
example:

```
    // This is just a comment
    // |...|...|...
    // |...|...|...
```

To access data like this, an *indent pattern* may be provided,
in which case leading sequences of the indent pattern will be ignored.

For example, the indent pattern `//` would then make all of these
lines equivalent:

```
// This is a 4-line table when the indent pattern is //
|...|...|...
    |...|...|...
    // |...|...|...
    //  //      |...|...|...
```

Whitespace on either side of the indent pattern is always ignored.

The indent pattern can be set via `psv -i <pattern>` or
`psv.Table.SetIndent(pattern)`

### Finding Data

After removing [indentation](#indentation), `psv` classifies each line
as one of:

    - [Table Rows](#table-rows)
    - [Rulers](#rulers)
    - [Text Lines](#text-lines)

#### Table Rows

Table rows *must* begin with a `|` (pipe)

#### Rulers

A ruler, or separation line, is any line which...

    - *only* contains a combination of the characters
      `|`, `:`, `#`, `*`, `+`, `=`, `~`, `-` or whitespace
    - *only* has *at most* 4 unique characters
    - is *not* an empty table row
      (consisting of just a combination of `|` and whitespace characters)


#### Text Lines

Any lines which do *not* begin with a `|` are ignored, but they are
stored in the `psv.Table.TextLines` slice.

## Writing PSV Tables

If no table rows are found, the original text is left unchanged.



- `psv` *only* formats psv tables

- *table rows* are


### Data Rows

- Always begin with a `|` character (after optional indentation)
  - Are indented to match the indentation of the first data row or ruler

- The number of columns in each row *do not have to be the same in every row*
  - Columns are added so that all columns appear from the first to the last row of the table.

- Columns may be empty
  - Empty *trailing columns* are always removed automatically
  - `psv -squash` or the `psv.Table.Squash` option may be used to *squash* empty
    columns and multiple empty rows
    - (one empty row in each series of empty rows are kept)

- Columns do not have to align vertically
  - That's exactly what `psv` does *for you* :-)
  - Whitespace around `|` characters is always re-*jiggered* so that the columns line up nicely
  - Whitespace within a cell's data is *never* removed

- ***Known issue*** it is not currently possible to include a `|` character in a cell's data.
  - This restriction should be removed in a future version of `psv`

- All data rows are available via the `psv.Table.Data` struct

Example:

```
|hello
|           what's up              |           doc
| we're going to | outer s p  a   c   e    !
|too much?||||see|||||||||||
```
becomes:

```
| hello          |                           |  |  |     |
| what's up      | doc                       |  |  |     |
| we're going to | outer s p  a   c   e    ! |  |  |     |
| too much?      |                           |  |  | see |
```

### Sections

- consecutive data rows are automatically grouped into _sections_
  - a section is any consecutive block of data rows, separated by any non-data
    rows (i.e. rulers or decorative text)
  - sections are identified by an integer number
    - the first line of data starts section `1`
  - sections _may_ be preceeded by a text line defining the section's name.
  - when sorting table data, the rows in each section are sorted separately.
  - e.g.
    ```
    tbl := psv.TableFromString(`
                               | good                             | block 1
                               | -------------------------------- |
                               | honey                            | block 2
                               | money                            |
                               | all other things ending in -oney |
                               | -------------------------------- |
                               | bad                              | block 3
                               | -------------------------------- |
                               | sad                              | block 4
                               | mad                              |
                               | evil                             |
                               `)
    for _, s := range tbl.Sections() {
      for _, r := range tbl.DataRows[s.start:s.end] {
        // do something with the data from this section
      }
    }
    ```

### Named Columns

Any section that has just _**one data row**_, and is _**followed by a ruler**_,
is assumed to be a _**heading**_.

### Sorting

- table data _can_ be sorted, optionally, by `psv`
  - `psv` only sorts table rows when called with any of the options
    - `-sort` or `-s`
    - `-by` `<column, ...>`
      - implies `-sort`
    - `-in` `<section, ...>`
      - implies `-sort`

### Squashing

Normally, `psv` will remove *trailing columns* automatically so you don't have
to worry about counting `|`'s.

However, if you have a table with unwanted, empty columns or rows _**within**
the table_, `psv` can also remove them for you, you just have to ask.

If you set `Table.Options.Squash = true`, or use the `-squash` command line option,
`psv` will...

- remove all empty columns
    - *Assumption:* empty columns have no value
- squash groups of *consecutive* ***empty table rows*** into a single, empty row per group
    - *Assumption:* you might want to keep an empty row to e.g. for a "test with no data"
- squash groups of *consecutive* ***empty lines*** into a single, empty line per group
    - *Assumption:* empty lines are usually for visual effect. No empty lines, no effect
- the difference between *empty rows* and *empty lines* being
    - ***empty table rows*** have `|` separated columns but no data
    - ***empty lines*** are lines without any data (after ignoring the current [indent](#indentation))
    - ***rulers*** are never considered *empty*
- note that a group of empty table rows followed by a group of empty lines is
    considered to be *two, separate groups* and will be squashed into a single
    empty row and a single empty line.

Squashing is an *all or nothing* option. When *squashing* is active, *all* empty
columns and rows are squashed. Otherwise no squashing takes place, except for
trailing empty columns.

#### Example

**input**
```
+--------------------------------------------+
| one |       |  | two |  |  | three | | | | |
| --- | ----- |  | --- |  |  | ----- | | | | |
|     |       |  |     |  |  |       | | | | |
|     |       |  |     |  |  |       | | | | |
|     |       |  |     |  |  |       | | | | |



| hi  | there |  |     |  |  |       | | | | |
```

**output** of `psv -squash`
```
+---------------------------+
| one |       | two | three |
| --- | ----- | --- | ----- |
|     |       |     |       |

| hi  | there |     |       |
```


### Rulers
#### (or *separator lines*)

Rulers are simple text lines only, but can help separate parts of your table if
needed.

As such, rulers are not part of the `psv.Table.Data` array when parsed,
but are, instead stored in `psv.Table.TextLines`.

Rulers, like data rows, may appear anywhere within the text provided to `psv`,
even if they are not directly adjacent to any data rows. This makes drawing outer
boxes easy, just put the top- and bottom rulers wherever you want them to be.

To create a ruler, add a line containing *only* the characters `|`, `:`, `#`,
`=`, `~`, `-`, `+` or whitespace.

The presence of any other characters will cause the line to be treated as a
data row (if the first character is `|`) or as plain text.

There are two ways to specify how your ruler should look:

1. via a 1- to 4-character ***format specification***
   - each character specifies the appearance of one aspect of the ruler.
   - in order, the characters are:
     1. the vertical lines used for the *outer border* of the table
     2. the padding character used on either side of vertical lines
     3. the horizontal line character
     4. the vertical line to use *between columns*
   - ***Special case:*** the `|` pipe character is *never* used for padding or horizontal lines!

Example:

```
+--
| one | two | three | fourty-two |
|=
| -
|  :
|--+
+--
```

becomes:

```
+-----+-----+-------+------------+
| one | two | three | fourty-two |
|=====|=====|=======|============|
| --- | --- | ----- | ---------- |
|     :     :       :            |
|-----+-----+-------+------------|
+-----+-----+-------+------------+
```

Special case:

Lines that only contain `|`'s are treated specially:

Example:

```
+-----+-----+-------+------------+
| one | two | three | fourty-two |
|=====|=====|=======|============|
|
||
| |
|||
|  |
||||
```

becomes:

```
+-----+-----+-------+------------+
| one | two | three | fourty-two |
|=====|=====|=======|============|
|     |     |       |            |
|     |     |       |            |
|     |     |       |            |
|     |     |       |            |
|     |     |       |            |
|     |     |       |            |
```

2. or via a ***template ruler***
   - if more than 4 characters are present, `psv` tries to use the line as a template.
   - the first two characters are the *outer border* and *padding* characters
   - following characters are assumed to be a series of horizontal line
     characters and possibly some individual column separators.
     - ***notes:***
       - not every constellation of column widths can be parsed unambiguously.
       - hand-made templates are ***easy to get wrong***.
       - rulers which were previously generated by `psv` are highly likely to be reproduced faithfully.
       - if in doubt, replace the template row with a simple format specification, as they are unambiguous.

Example:

```
+---------+----------+-------+------------+
| one | two | three | fourty-two |
|==|===|===|====|
|  | - | - | -- |
|  :   :   :    |
|--+---+---+----|
+--+---+---+----+
```

becomes:

```
+-----+-----+-------+------------+
| one | two | three | fourty-two |
|=====|=====|=======|============|
| --- | --- | ----- | ---------- |
|     :     :       :            |
|-----+-----+-------+------------|
+-----+-----+-------+------------+
```

### Additional Text

Any lines which are not data rows or rulers are simply *text*.

Text lines are never modified, *except* that their indentation will be changed
to match the data rows if the text line is *within* the table.

Lines which have *only* whitespace are reduced to an empty line.

Example:

```
                Work Plan
    + -
    | task | due date | done |
    + -

Important:
| read 'the net' | today

                BOOOORRRING
    | take the rubbish out | never | sure
    + -

What else could I do?
```

becomes:

```
                Work Plan
    + -------------------- + -------- + ---- +
    | task                 | due date | done |
    + -------------------- + -------- + ---- +

    Important:
    | read 'the net'       | today    |      |

    BOOOORRRING
    | take the rubbish out | never    | sure |
    + -------------------- + -------- + ---- +

What else could I do?
```

## Markdown Tables

Github's variant of markdown includes a table format, which is similar, but
different to the table formatting used by `psv`.

However, in order to make `psv` also work with markdown tables, the different
rules are applied.

***If...***
1. **the *entire* table consists of consecutive, adjacent table rows**
    - empty lines before and after the table are still ignored
    - but no empty or non-table lines exist within the table
2. ***and* the first table row is a *data row***
    - containing `|`, whitespace and non `-` or `:` characters
3. ***and* the second table row is a [markdown delimeter row](...)**
    - containing *only* `|`, whitespace, `-` and, if needed, `:` characters for column alignment
        - markdown rejects the entire table the 2nd line does not match the
        regular expression `\|?([\s\pZ]*:?-+:?[\s\pZ]*)?(\|[\s\pZ]*[\s\pZ]*:?-+:?[\s\pZ]*)?\|?`
        - i.e.:
            - there *must* be at least 1 consecutive `-` between each `|` delimeter
            - `:` may *only* appear at the ends of `-` sequences - no whitespace allowed,
            i.e. `:---`, `---:` or `:--:` (with any number of `-`'s)
            - whitespace *may* be used between `|`'s and thw previous / next delimeter character
    - ***`psv` exception:*** the number of columns *do not need to be the same* as the 1st row!
        - missing delimeter columns will be added by `psv` as `| -- |`
        - additional delimeter columns will cause the table to be extended(!)
        - the [squash](...) option may be used to remove empty columns

If all of the above conditions are met, then the second line will be
interpereted as a markdown delimeter, allowing for specific content alignment
within each column.

**A markdown table:**
| default | left | center | right |
| ------- | :--- | :----: | ----: |
| 1       | 2    | 3      | 4     |
|         |      |        |       |
|         |      |        |       |
|         |      |        |       |
|         |      |        |       |
| ------- | ---- | --:--- | ----- |
| 1       | 2    | 3      | 4     |
