# The `psv` Command

The `psv` command is designed to re-format *chunks* of text containing *pipe
separated values* into a table of aligned, *pipe separated columns*.

The `psv` command _always_ reads text from `STDIN`, formats any table rows it finds and and
writes its formatted output to `STDOUT`.

- this makes `psv` ideal for use with typical unix text editors
- `psv` is expected to work on *parts* of files, so formatting *entire files*
  (by filename) is ***deliberately not supported***.

The typical workflow is:

1. create or edit some [*table like*](formatting.md) lines of text in your text editor
2. format the table by piping it through `psv` 
3. repeat...

The input lines are parsed and formatted according to `psv`'s [formatting
rules](formatting.md).

***Note*** that all lines read from `STDIN` are assumed to belong to ***the same table***.
This is a deliberate feature! This way, you can add comments, headers etc.
*inside* your table without having to manually align columns which would otherwise
span separate tables.

In general, the [formatting rules](formatting.md) used by `psv` have been
chosen to minimise the need for configuration. Under normal circumstances, no
customisation or configuration options should be required.

However, the options below are provided for situations where unforseeable behaviour (such
as tables embedded in comments) or destructive actions (such as squashing empty rows
and columns) might be required.

## Command Line Options

**Behavioural Options**

- `-i <indent>`
  - use a custom indent
  - `<indent>` may be...
    - a number representing the desired indent level, between 0 and 63
    - a string representing some comment characters to be removed and/or replaced
    - a whitespace string used to indent all formatted rows
  - see the [formatting rules](formatting.md/#indentation) for details of how table indentation works

- `-sort` | `-s`
  - sort data rows.
  - see [sorting](sorting.md) for details.
  - see also `-by` and `-in`

- `-by` `<columns, ...>`
  - implies `-sort`
  - indicate which _columns_ should be used for sorting.
  - columns may be specified by name or index.
    - column names are defined by the first row of data, or any single row of data.
      - the column names provided with `-by` are matched _case-**in**sensitively_
    - column indexes are simple the numeric offset, with `1` being the left-most column.
    - column names and indexes may be mixed.
    - a `~` prefix before any column name or index causes the order in that column to be reversed.
      - e.g. `-by "~date"` to sort the `date` column with the most recents ISO 8601 dates first
    - a `#` prefix before any column name of index causes the sorting be performed numerically (i.e. `2 < 10`)
      - e.g. `-by "#temperature"` to sort the table from coldest to hottest
        - _**Known BUG:**_ signed values are not handled properly :cry:
    - a `@` prefix before any column name of index causes the sorting be performed lexographically (i.e. `10 < 2`)
    - by default, rows are sorted using all columns from left to right, using _**numeric**_ order (i.e. `2 < 10`!)
    - the prefixed may be mixed, e.g.:
      - `-by "~#num,@name` first sort the `num` column from largest to
        smallest, numerically, and then `name` column, lexographically, to futher
        sort rows with the same `num` value.
    - using a flag without a column name or number applies the flag to all columns being sorted
      - `-by "~"` simply reverse the sort order for all columns
    - **warning:**
      - unix shells _like_ to interpret `~` as "a user's home directory" and `#` as "a comment"
      - you should always quote your column specifications

- `-in` `<sections, ...>`
  - implies `-sort`
  - indicate which _sections_ should be used for sorting
  - sections may be specified by name or index.
    - section names are defined by the most recent single-line text line (surrounded
      by rulers, data rows or empty lines) before a block of data rows.
    - a table may have any number of sections.
    - there may be any number of text lines before, between or after any block
      of data rows.
    - section indexes are simple numeric offsets, starting at 1 and increasing
      by 1 with each independent block of data rows (separated by blank lines,
      text or rulers)
    - section names or numbers may also be preceeded by the same prefixes
      described in the columns section above

- `-squash`
  - *remove* empty columns and rows
  - see the [formatting rules](formatting.md#squashing) for more information

**Informational Options**

- `-help` | `-h`
  - display `psv` usage instructions and exit

- `-version` | `-v`
  - display the version of `psv` and exit
