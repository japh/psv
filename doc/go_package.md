# The `psv` Go Package

## Generating PSV Tables From Scratch

See [`features/generate_table_from_data_test.go`](../features/generate_table_from_data_test.go)

## Using PSV Tables to Generate PSV Tables

See [`features/generate_table_from_table_test.go`](../features/generate_table_from_table_test.go)

## Using PSV Tables for Test Data

Contrast [`features/unit_test_with_go_data_test.go`](../features/unit_test_with_go_data_test.go)

against [`features/unit_test_with_psv_data_table_test.go`](../features/unit_test_with_psv_data_table_test.go)

--------------------------------------------------------------------------------

[Go](https://go.dev) encourages programmers to write lots of tests
while programming.

Live documentation is of course available via godoc, for example:

```
godoc -http :6060 -play -notes &
```

Which is great.

However, a typical test function often looks similar to JSON:

```
func TestMaths(t *testing.T) {

    testCases := []testCase{
        {"a", map[string]int{"a": 5}, 5, "an expressiong without operators"},
        {"a + b", map[string]int{"a": 5, "b": 3}, 8, ""},
    }

    for _, tc := range testCases {
        ...
    }
}
```

The use of a PSV table greatly improves the reader's ability to scrutinise the data being tested:


```
func TestMathsWithTable(t *testing.T) {

    // Note: additional space and decorative formatting
    testCases, _ := psv.TableFromString(`

        | expression | values  | result | comment                          |
        | ---------- | ------- | ------ | -------------------------------- |
        | a          | a:5     | 5      | an expressiong without operators |
        | a + b      | a:5 b:3 | 8      |                                  |

        `)

    // Use Data[1:] because the 1st row contains columnn headers and the
    // second row is a ruler (considered to be _decorative text_) and is not
    // included in the data
    for _, tRow := range testCases.Data[1:] {

        // convert strings into our structure
        tc := testCase{}
        tc.expr = tRow[0]
        tc.result, _ = strconv.Atoi(tRow[2])
        tc.comment = tRow[3]
        // embedded values are possible!
        tc.values = map[string]int{}
        for _, v := range strings.Fields(tRow[1]) {
            s := strings.Split(v, ":")
            tc.values[s[0]], _ = strconv.Atoi(s[1])
        }

        // same as TestMaths()...
        ...
    }
}
```

Remember, we can use the `psv` command line tool to help keep the
table *pretty*. Inside our test code, we can now use the *same psv
code* to parse our tables and provide us with a `[][]string` table.
(`[][]string` is go's way of saying *'a 2-dimensional array of
strings'*)

The `psv.Table` struct separates your data into two sections.

    - `Data` is the `[][]string` table of data.
      - The `Data` table *only* contains data from the rows beginning with `|`!
      - All data has leading and trailing white-space removed
      - [blank lines], [rulers] and non-tabular [text] are *not*
        stored in the `Data` slice at all.

    - `TextLines` is a `[]TextLine` slice, which provides access to the
      non-data lines of the string.


## API

### Summary

- [`New() *Table`]()
- [`NewTable() *Table`]()   _deprecated_
- [`TableFromString(string) (*Table, err)`]()
- [`(*Table)DecodeString(string)`]()
- [`(*Table)Encode() string`]()
- [`(*Table)Read(Scanner) error`]()
- [`(*Table)SetIndent(string)`]()
- [`(*Table)SetSquash(bool)`]()
- [`(*Table)Write(Writer)`]()

### Import

`import "codeberg.org/japh/psv"`

### Table Creation & Configuration

#### `New()`

NewTable creates a new `Table` struct which can be used for
reading or writing PSV tables.

#### `(*Table) SetIndent( string )`

By default, tables have no specific indent.

If an indent was detected when reading a table, it will also be used
for writing the table.

By setting the indent specifically, it is possible to skip programming
comments while reading the table and explicitly specify the indent to
use when writing the table.

see [Indenting](formatting.md#indenting)

#### `Table.Squash bool = true`

When set to true, sequences of empty table rows or text lines are
reduced to a single line and empty columns are removed entirely from
the resulting table.

See [Squashing](formatting.md#suashing)

### Reading Tables

#### `(*Table) Decode( string )`

Build a table's Data from the text provided.
Internally, this just calls `(*Table)Read( Scanner )`

#### `(*Table) Encode() string`

Encode convert's a table's data and text lines into a single, multi-line
formatted string. Internally, this just calls `(*Table)Write( Writer )`

#### `(*Table) Read( Scanner ) error `

Read parses lines of text

#### `(*Table) Write( Writer)`

### Accessing Table Data

#### `Table.Data [][]string`

Table.Data is a simple, 2-dimensional slice of strings.

The Data slice produced by `Read()` is guaranteed to have a constant
row-size (all rows have the same number of elements).

When creating data, rows may have different lengths. The Table created
by `Write()` will be formatted so that missing elements appear as empty
cells.

#### `Table.TextLines []*TextLine`

`Read()` tracks all [Additional text]() and [rulers]() as
[`TextLine`](#textline-struct) structs in the `Table.TextLines`
slice, sorted by line number.

There is **no _automatic_ synchronisation of text lines and table data!**

Decorative text lines are interspersed before, between and after the table
rows, making it possible to keep the original structure of all input lines that
are not data rows.

When creating tables, decorative text lines can be placed anywhere.

e.g.:

```
tbl := psv.NewTable()
tbl.Data = [][]string{
    {"Date", "Temperature °C"},
    {"2022-01-01", "5"},
    {"2022-01-02", "-2"},
    {"2022-01-03", "3"},
    {"2022-01-04", "1"},
}
tbl.Decorations = []*psv.Decoration{
    &psv.Decoration(Line:1, Text:"Top Temperatures (*)"}, // title first
    &psv.Decoration(Line:2},                              // empty line
    &psv.Decoration{Line:3, Ruler:"+-"},                  // top table border
    &psv.Decoration{Line:5, Ruler:"| ="},                 // header row separator
    &psv.Decoration{Line:10, Ruler:"+-"},                 // bottom table border
    // empty line after the table
    &psv.Decoration{Line:12, Text:"(*) detected by the 'finger out the window' method"},
}
tbl.SetIndent("4")
fmt.Print( tbl.Encode() )
```

would produce the table:

```
Top Temperatures (*)

    +------------+----------------+
    | Date       | Temperature °C |
    | ========== | ============== |
    | 2022-01-01 | 5              |
    | 2022-01-02 | -2             |
    | 2022-01-03 | 3              |
    | 2022-01-04 | 1              |
    +------------+----------------+

(*) detected by the 'finger out the window' method
```

#### `Decoration struct`

```
type Decoration struct {
	Line   int    // 1-based number of this line's position in the resulting table
	Indent string // only used to re-construct original indentation outside a table's bounds
	Text   string // un-indented text to be inserted into the table, may be ""
	Ruler  string // ruler specification, see Ruler type
}
```

