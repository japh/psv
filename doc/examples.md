# Examples

This page just contains a collection of examples, to show how `psv` might be used.

Each example has two parts:
- the original *input* text
- the formatted *output* text

In each case, the *output* is produced by `psv` when it is given the *input*.

Similar to the shell command `psv < input > output`, except that normally we
*pipe* the input from our editor into `psv`, which replaces the input with
`psv`'s output.

## Index

- [A Basic Table](#a-basic-table)
- [Text-Based Tables: A Quick Comparison](#text-based-tables-a-quick-comparison)
- [Markdown Tables](#markdown-tables)
- [Gherkin Scenarios](#gherkin-scenarios)
- [Tables In Comments](#tables-in-comments)
- [A Table With Sections](#a-table-with-sections)
- [Squashing Empty Columns And Rows](#squashing-empty-columns-and-rows)

## A Basic Table

Let's start with a very simple table, as you might find in a
[gherkin](https://cucumber.io/docs/gherkin/reference/) scenario description.
Whether the program (or person) reading this table considers the first row
to be a header or data, is *up to them*.

Either way, `psv` doesn't care or need headers... it just aligns everything in
to neat columns:

**input**
```
XOR Logic:
|0|0|0
|no|yes|yes
|true|false|true
|on|on|off
```

**output**
```
XOR Logic:
| 0    | 0     | 0    |
| no   | yes   | yes  |
| true | false | true |
| on   | on    | off  |
```

## Text-Based Tables: A Quick Comparison

Let's focus on *readability* for a bit.
The *content* of each of the following examples is identical, but the
formatting makes a huge difference when it comes to *understanding* what was
intended, *identifying problems* or *making changes*.

I have become quite fond of writing my unit tests as a table of test data,
followed by a loop which runs a common set of tests for each row of data.

Each programming language has it's own style of 'coding a table of data',
however, I often end up with something akin to JSON:

```
yumminess = [
            {name: "Schnitzel",        region: "Germany", popularity: 5, variations: 1,   difficulty: 5},
            {name: "Pizza",            region: "Italy",   popularity: 4, variations: 100, difficulty: 2},
            {name: "Spaghetti",        region: "Italy",   popularity: 3, variations: 5,   difficulty: 1},
            {name: "Quiche",           region: "France",  popularity: 1, variations: 10,  difficulty: 1},
            {name: "Fried Rice",       region: "Asia",    popularity: 3, variations: 50,  difficulty: 1},
            {name: "Chicken Jalfrezi", region: "India",   popularity: 3, variations: 3,   difficulty: 5},
            ]
```

or CSV (Comma Separated Values):

```
yumminess = 'name,region,popularity,variations,difficulty
Schnitzel,Germany,5,1,5
Pizza,Italy,4,100,2
Spaghetti,Italy,3,4,1
Quiche,France,1,10,1
"Fried Rice",Asia,3,50,1
"Chicken Jalfrezi",India,3,3,5'
```

:anguished: Wait! That's worse!

Let's see what `psv` can do:

```
yumminess = '
            +------------------+---------+------------+------------+------------+
            | name             | region  | popularity | variations | difficulty |
            | ================ | ======= | ========== | ========== | ========== |

            Classic European:
            | Schnitzel        | Germany | 5          | 1          | 5          |
            | Pizza            | Italy   | 4          | 100        | 2          |
            | Spaghetti        | Italy   | 3          | 4          | 1          |
            | Quiche           | France  | 1          | 10         | 1          |

            Exotic:
            | Fried Rice       | Asia    | 3          | 50         | 1          |
            | Chicken Jalfrezi | India   | 3          | 3          | 4          |
            +------------------+---------+------------+------------+------------+

            Note: 72% of these statistics are probably 86% inaccurate,
            or completely made up :-)
            '
```

Ah! Much better! :tada:

Now, use this table directly for my tests, we just need to turn the table from text into a 2-dimensional array.
The `psv` [go package](go_package.md) makes this easy:

```golang
import (
        "codeberg.org/japh/psv"
        "testing"
        )

func TestPSVTable(t *testing.T) {
    testCases := psv.TableFromString(`
                                    // your table goes here
                                    ...
                                    `)

    // loop over each row of the table.
    // testCases[1:] just skips the header row
    for r, row := range testCases[1:] {
        // run tests using row [r] ...
        name   := row[0]    // e.g. "Schnitzel"
        region := row[1]    // e.g. "Germany"
        ...
    }
}
```

Done!

## Markdown Tables

_**Warning:** support for markdown tables has not yet been implemented!_

**input**
```markdown
| markdown | left | center | right |
| -------- | :--- | :----: | ----: |
| numbers | 1 | 2 | 3 |
```

**output**
```markdown
| markdown | left | center | right |
| -------- | :--- | :----: | ----: |
| numbers  | 1    |    2   |     3 |
```
 
## Gherkin Scenarios

By the way, `psv` is also ideally suited for formatting *Data Tables* in
[Gherkin](https://cucumber.io/docs/gherkin/reference/) files, which are a
central component of [Behaviour Driven Development
(*BDD*)](https://en.wikipedia.org/wiki/Behavior-driven_development).

**input**
```gherkin
Feature: produce cakes
  Scenario: bake a cake
    Given the ingredients
          | ingredient | amount |
          | flour | 2 cups |
          | eggs | 2 |
          | sugar | heaps  |
     When the ingredients have been mixed and kneaded
     Then the dough should be light and fluffy
```

**output**
```gherkin
Feature: produce cakes
  Scenario: bake a cake
    Given the ingredients
          | ingredient | amount |
          | flour      | 2 cups |
          | eggs       | 2      |
          | sugar      | heaps  |
     When the ingredients have been mixed and kneaded
     Then the dough should be light and fluffy
```

***Note*** `psv` kept the original formatting of everything before and after the
table. You don't have to give `psv` the *right* text, just ***some text***
containing **at most one** table.

## Tables In Comments

**input**
```
    // What if I want a table in my comments?
    //
    //  //  What happens here?
    //  //
    //  //  | Request | Errors? | Result
    //  //  | -+
// // | coffee! | no | 1 cup of coffee
        // // | tea | no | 1 pot of tea
// // | wine | we have no alcohol here | empty glass
```

In this case, `psv` needs to know that the `//` symbol needs to be skipped before looking for tables.

If you just run this through `psv` (without options), nothing will change.

`psv -i //` however, will do this:

**output**
```
    // What if I want a table in my comments?
    //
    //  //  What happens here?
    //  //
    //  // | Request | Errors?                 | Result          |
    //  // | ------- + ----------------------- + --------------- |
    //  // | coffee! | no                      | 1 cup of coffee |
    //  // | tea     | no                      | 1 pot of tea    |
    //  // | wine    | we have no alcohol here | empty glass     |
```

## A Table With Sections

Since `psv` only formats *the contents of a table*, anything before and after the table remains unchanged.
Non-table rows *within* the table are *indented* to match the table, but otherwise also left unchanged.

**input**
```
                Strange Words
    +---
    | Word | Meaning
    | =

    Short Words:
    | aghast | filled with horror or shock
    | agist | take in and feed (livestock) for payment
    | calyx | a cup-like cavity or structure

    Long Words:
    |compartmentalization|divide into discrete sections or categories
    |Gigantopithecus|a very large fossil Asian ape of the Upper Miocene to Lower Pleistocene epochs
    |instantaneously|instantly; at once
    |supercalifragilisticexpialidocious | extraordinarily good; wonderful
```

**output**
```
                Strange Words
    +---------------------------------------------------------------------------------------------------------------------+
    | Word                               | Meaning                                                                        |
    | ================================== | ============================================================================== |

    Short Words:
    | aghast                             | filled with horror or shock                                                    |
    | agist                              | take in and feed (livestock) for payment                                       |
    | calyx                              | a cup-like cavity or structure                                                 |

    Long Words:
    | compartmentalization               | divide into discrete sections or categories                                    |
    | Gigantopithecus                    | a very large fossil Asian ape of the Upper Miocene to Lower Pleistocene epochs |
    | instantaneously                    | instantly; at once                                                             |
    | supercalifragilisticexpialidocious | extraordinarily good; wonderful                                                |
```

## Squashing Empty Columns And Rows

With the `-s` *(or `psv.Table.Options.Squash`)* option, we can remove _**all** empty columns_, and
_**duplicate**, squential empty rows_.

Empty columns are assumed to have no meaning.

A single empty row *may* be used to indicate a test with no data, or be used
for visual effect.

So, *squashing* works like this:

**input**
```


empty lines before the table are uneffected




    | one | two |      |  |   | three



    | -
    |
    |
    |one
    |
    |



    hello


    | one | two |||| three
    | four |
    | | | |
    | | | | | |
    |
    | | | | | | bye


empty lines after the table are uneffected
```

**output** of `psv -s`
```


empty lines before the table are uneffected




    | one  | two | three |

    | ---- | --- | ----- |
    |      |     |       |
    | one  |     |       |
    |      |     |       |

    hello

    | one  | two | three |
    | four |     |       |
    |      |     |       |
    |      |     | bye   |


empty lines after the table are uneffected
```

