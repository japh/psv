# Markdown Tables

Markdown has it's own special rules which, of course, differ from `psv`'s
[formatting rules](formatting.md).

Notably:

- all lines of a table *must* be adjacent to each other
    - no blank lines are allowed
- the first two table rows are special
    - the first line *must* be a header row
    - the second line *must* be a delimeter row, with the same number of columns as the header row
- column data *may be left, right or center aligned*
    - `psv` now has a special add-on to recognise and handle this
- table rows do not need to start with a `|`
    - however, `psv` requires that every line begins with a `|`
    - the formatted table produced by `psv` will always have a `|` before the first column
