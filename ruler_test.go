package psv

import (
	"fmt"
	"regexp"
	"testing"
)

func TestEncodeRulers(t *testing.T) {
	testCases := []struct {
		format string
		widths []int
		ruler  string
	}{
		{format: "", widths: []int{2, 2}, ruler: "-----------"}, // not legal, but we don't want errors
		{format: "-", widths: []int{2, 2}, ruler: "-----------"},
		{format: "=", widths: []int{2, 2}, ruler: "==========="},
		{format: "- ", widths: []int{2, 2}, ruler: "-    -    -"},
		{format: "|-", widths: []int{2, 2}, ruler: "|----|----|"},
		{format: "| -", widths: []int{2, 2}, ruler: "| -- | -- |"},
		{format: "| -+", widths: []int{2, 2}, ruler: "| -- + -- |"},
		{format: "|=-+", widths: []int{2, 2}, ruler: "|=--=+=--=|"},
		{format: "|=-+", widths: []int{3, 4}, ruler: "|=---=+=----=|"},
		{format: "|=-+", widths: []int{3, 4}, ruler: "|=---=+=----=|"},
		// special cases
		//  - '|' (pipes) are *only* used as vertical separators
		//    - never as padding or horizontal lines
		//    - => therefor, and row consisting only of pipes becomes an empty data row
		{format: "|", widths: []int{3, 4}, ruler: "|     |      |"},
		{format: "||", widths: []int{3, 4}, ruler: "|     |      |"},
		{format: "|||", widths: []int{3, 4}, ruler: "|     |      |"},
		{format: "||||", widths: []int{3, 4}, ruler: "|     |      |"},
	}

	for tn, tc := range testCases {
		t.Run(fmt.Sprintf("test %d: %+v", tn, tc.format),
			func(t *testing.T) {
				r, _ := NewRuler(tc.format)
				result := r.Encode(tc.widths)
				if result != tc.ruler {
					t.Errorf("unexpected ruler\ngot:  %q\nwant: %q", result, tc.ruler)
				}
			})
	}
}

func TestDecodeRulers(t *testing.T) {
	testCases := []struct {
		input  string
		format string
		err    string
	}{
		// "" doesn't contain any ruler characters, should not be parsed as a ruler
		// "|" pipe-only => empty data row

		// ruler formats
		{input: "|-", format: "|--|"},
		{input: "|--", format: "|--|"},
		{input: "| -", format: "| -|"},
		{input: "| -+", format: "| -+"},

		// previously formatted rulers
		// {input: "|  |", format: "| -|"}, // no ruler characters => empty data row, not parsed as a ruler
		{input: "|  +  |", format: "|  +"},   // two empty cells, use default line char
		{input: "|  + - |", format: "| -+"},  // empty cell followed by a cell with 1 char
		{input: "|  + = |", format: "| =+"},  // empty cell followed by a cell with 1 char, get line char from 2nd cell
		{input: "| = + = |", format: "| =+"}, // two cells with 1 char
		{input: ":  :", format: ":  :"},      // alternative vertical lines, default horizontal line
		{input: ":==:", format: ":==:"},      // alternative vertical lines, padding only
		{input: ":==+==:", format: ":==+"},   // alternative vertical lines, padding only
		{input: ":=-=+==:", format: ":=-+"},  // alternative vertical lines, padding only

		// unparsable input
		{input: "| = + - |", err: "too many unique characters"},
		{input: "|-:+-", err: "mismatching format characters"},
		{input: "|:=*+-", err: "too many unique characters"},
		{input: "|-|-|-|", err: "does not have plausibly spaced columns"},
	}

	for tn, tc := range testCases {
		t.Run(fmt.Sprintf("test %d: %+v", tn, tc.input),
			func(t *testing.T) {
				r, err := NewRuler(tc.input)
				if (err == nil) != (tc.err == "") {
					t.Errorf("unexpected error\ngot:  %s\nwant: /%s/", err, tc.err)
					return
				}
				if tc.err != "" {
					if ok, _ := regexp.MatchString(tc.err, err.Error()); !ok {
						t.Errorf("unexpected error\ngot:  %s\nwant: /%s/", err, tc.err)
						return
					}
				}
				result := r.format()
				if result != tc.format {
					t.Errorf("unexpected ruler\ngot:  %q\nwant: %q", result, tc.format)
				}
			})
	}
}

// create a ruler using a format
// create a second ruler, that takes the first ruler as its input
// compare the second ruler to the first
// compare the second format to the first and initial formats
func TestRulerRoundTrip(t *testing.T) {
	testCases := []struct {
		format string
	}{
		{format: ""},
		{format: "-"},
		{format: "+"},
		{format: ":"},
		{format: "|-"},
		{format: "|--"},
		{format: "| -"},
		{format: "|- "},
		{format: "| -+"},
		{format: "|--+"},
		{format: "|- +"},
		{format: "|-+ "},
		{format: "|-+-"},
		{format: "|-++"},
		{format: "|++-"},
		{format: "|+-+"},
	}

	cols := [][]int{
		{},        // empty row, not a table
		{0},       // minimal table with no data
		{1},       // minimal table with 1 character in 1 column
		{0, 1, 2}, // improbable - empty columns make little sense
		{1, 1, 1}, // highly unlikely
		{1, 2, 3}, // more likely
		{2, 2, 2},
		{5, 4, 6}, // typical
	}

	for tn, tc := range testCases {
		for _, c := range cols {
			t.Run(fmt.Sprintf("test %d: %+v with %+v", tn, tc.format, c),
				func(t *testing.T) {

					r1, _ := NewRuler(tc.format)
					r1Row := r1.Encode(c)

					r2, err := NewRuler(r1Row)
					if err != nil {
						t.Errorf("parsing generated ruler caused an unepected error\ngot:  %q", err)
						return
					}
					r2Row := r2.Encode(c)

					if r2Row != r1Row {
						t.Errorf("failed ruler round-trip test\nfmt:  %q\ngot:  %q\nwant: %q", tc.format, r2Row, r1Row)
					}
				})
		}
	}
}

func ExampleRuler_Encode() {
	columnWidths := []int{1, 2, 3, 4}
	ruler, _ := NewRuler("| -:")

	fmt.Println(ruler.Encode(columnWidths))

	// output:
	// | - : -- : --- : ---- |
}
