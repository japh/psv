package psv

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// Indenter is used to track how a table was, and should be, indented.
//
// When decoding tables, the Table's Indenter is used to automatically detect
// and track the indent of the first line of the table.
//
// When encoding tables, the Table's Indenter provides the desired indentation
// for each table row.
//
// In addition, the Indenter is also responsible for detecting and
// reconstructing a consistent 'comment style', if it was provided with a
// non-whitespace indent pattern.
//
type Indenter struct {
	matcher       *regexp.Regexp // regular expression used to remove indents while decoding input
	matchedIndent string         // buffer for indent found in the current text line
	indent        string         // static string prepended to each formatted table line
	prefix        string         // static string to use for empty lines (indent without trailing spaces)
	isFinal       bool           // indicate if the indent should still be derived from input lines
}

// NewIndenter creates a new Indenter
func NewIndenter() *Indenter {
	return &Indenter{
		matcher: regexp.MustCompile(`^[\s\pZ]*`),
	}
}

func (i *Indenter) Clone() *Indenter {
	return &Indenter{
		matcher:       i.matcher, // OK - regexp's are immutable
		matchedIndent: i.matchedIndent,
		indent:        i.indent,
		prefix:        i.prefix,
		isFinal:       i.isFinal,
	}
}

// SetIndent explicitly sets a desired indent to use.
//
// If SetIndent has been called, the Indenter will match indents according to
// the provided pattern, and will re-create indents using the provided pattern.
//
// Special situations:
//
//  - the pattern is just whitesspace (including "")
//      - leading whitespace will be removed when decoding
//      - the indent string will be used, as provided, when encoding
//
//  - the pattern provided is just a number `n`
//      - leading whitespace will be removed when decoding
//      - the encoding indent will be `n` of spaces
//
//  - the pattern contains non-whitespace characters
//      - any instances of the non-whitespace pattern will be skipped at
//        the beginning of each line
//      - if the pattern has no leading or trailing whitespace
//          - the skipped indent from the first table row will be re-used to
//            indent all table rows
//      - if the pattern has a leading whitespace
//          - the indent string will be used as provided
//          - the whitespace padding between the indent and the start of
//            text will be retained from the first table row
//      - if the pattern has a trailing whitespace
//          - only the whitespace before the first non-whitespace character
//            found will be re-used for indenting table rows
//          - the indent string will then be used as provided
//      - if the pattern has leading and trailing whitespace
//          - then the indent will be used, unmodified, for each table row
//
func (i *Indenter) SetIndent(pattern string) error {

	// convert a simple number into that many spaces
	if width, err := strconv.ParseUint(pattern, 0, 6); err == nil {
		pattern = strings.Repeat(" ", int(width))
	}
	if isNum, _ := regexp.MatchString(`^[+-]?\d+$`, pattern); isNum {
		return fmt.Errorf("indent must be an unsigned integer <64 or non-numeric string")
	}

	i.indent = pattern
	i.isFinal = true

	// split into leader + prefix + padding if the provided indent contains
	// non-whitespace characters
	re := regexp.MustCompile(`^([\s\pZ]*)([^\s\pZ].*?)([\s\pZ]*)$`)
	m := re.FindStringSubmatch(i.indent)
	if m != nil {
		leader := m[1]
		prefix := m[2]
		padding := m[3]

		// define a custom regex to remove the user's indent when decoding text.
		// allow for lines which don't (yet) have the prefix.
		indentPattern := fmt.Sprintf(`^((?:[\s\pZ]*%s)*)([\s\pZ]*)`, regexp.QuoteMeta(prefix))
		i.matcher = regexp.MustCompile(indentPattern)

		// allow detection of indent level from input text if user only
		// provided a prefix (without leader or padding)
		if leader == "" && padding == "" {
			i.isFinal = false
		} else {
			i.prefix = leader + prefix
		}
	}

	return nil
}

// FindIndent checks if the beginning of a line of text matches the current
// indent pattern and returns the indent string.
//
// The returned string is used to then split the line into <indent> and <text>,
// and may be used to re-construct the original line if it lies before or after
// the table's rows
func (i *Indenter) FindIndent(text string) string {
	i.matchedIndent = i.matcher.FindString(text)
	return i.matchedIndent
}

// FinalizeIndent is called while decoding, to indicate that the first table
// row has been detected.
//
// The most recently detected indent will then be retained and used for table
// row encoding.
func (i *Indenter) FinalizeIndent() {
	if i.isFinal {
		return
	}

	m := i.matcher.FindStringSubmatch(i.matchedIndent)
	if len(m) == 3 {
		// user provided a prefix which may or may not have been present
		// => always combine the user's prefix with the spacing found in text
		prefix := m[1]
		padding := m[2]
		if prefix == "" {
			prefix = i.indent
		}
		if padding == "" {
			padding = " "
		}
		i.prefix = prefix
		i.indent = prefix + padding
	} else {
		// just use the padding we found
		i.indent = i.matchedIndent
	}

	i.isFinal = true
}

// Indent returns the indent to be used when encoding a table row
func (i *Indenter) Indent() string {
	return i.indent
}

// Prefix returns the non-whitespace part of the indent pattern provided
func (i *Indenter) Prefix() string {
	return i.prefix
}

// String is only used for 'nice output' while debugging
func (i *Indenter) String() string {
	finalState := "default"
	if i.isFinal {
		finalState = "finalised"
	}
	return fmt.Sprintf("%q (%s)", i.indent, finalState)
}
