package psv_test

import (
	"fmt"
	"regexp"
	"strings"
	"testing"

	"codeberg.org/japh/is"
	"codeberg.org/japh/psv"
)

type dataCheck struct {
	r, c int
	d    string
}

func (c *dataCheck) String() string {
	return fmt.Sprintf("[%d,%d] %q?", c.r, c.c, c.d)
}

func TestDecodeScanLine(t *testing.T) {
	testData := `

            This is an example PSV table

                |any|line|starting|with|a|pipe|is|a|table|row
                -
                | well | formed | lines | are | appreciated |

                empty lines and comments are encouraged
    weird indentation is also not a problem
                | -- | --- | ------ | --- | -------- |
                | as | are | rulers | for | emphasis |
                +------+

        special characters are OK: | - + and = are typical ruler characters
        but don't have any effect when non-ruler characters are also present

                | "|" | pipes | within | tables | are | not | available | yet
                | sorry ! | really | :-) |

        additional text after a table is left untouched

                `

	expect := struct {
		dataRows  int
		dataCols  int
		textLines int
		firstRow  int
		lastRow   int
		indent    string
		data      []dataCheck
	}{
		dataRows:  5,
		dataCols:  10,
		textLines: 18,
		firstRow:  5,
		lastRow:   20,
		indent:    `                `,
		data: []dataCheck{
			{0, 0, "any"},     // top left cell
			{0, 9, "row"},     // top right cell
			{4, 0, "sorry !"}, // bottom left cell
			{4, 2, ":-)"},     // bottom right cell
		},
	}

	tbl := psv.NewTable()
	tbl.DecodeString(testData)

	if len(tbl.Data) != expect.dataRows {
		t.Errorf("bad data row count\ngot:  %d\nwant: %d", len(tbl.Data), expect.dataRows)
	}

	if len(tbl.TextLines) != expect.textLines {
		t.Errorf("bad text line count\ngot:  %d\nwant: %d", len(tbl.TextLines), expect.textLines)
	}

	if tbl.Indent() != expect.indent {
		t.Errorf("bad indent\ngot:  %q\nwant: %q", tbl.Indent(), expect.indent)
	}

	for r, row := range tbl.Data {
		if len(row) != expect.dataCols {
			t.Errorf("bad column count in row %2d\ngot:  %2d\nwant: %2d", r, len(row), expect.dataCols)
		}
	}

	rows, cols, widths := tbl.AnalyseData()
	if rows != expect.dataRows {
		t.Errorf("inconsistent row count\ngot:  %d from AnalyseData()\nwant: %d", rows, expect.dataRows)
	}
	if cols != expect.dataCols {
		t.Errorf("inconsistent column count\ngot:  %d from AnalyseData()\nwant: %d", cols, expect.dataCols)
	}

	td := tbl.Data
	for _, c := range expect.data {
		if c.r >= len(td) {
			t.Errorf("not enough rows\ngot:  %d rows\nwant: %s", len(td), c.String())
			continue
		}

		if c.c >= len(td[c.r]) {
			t.Errorf("not enough columns\ngot:  %d cols in row %d\nwant: %s", len(td), c.r, c.String())
			continue
		}

		if c.d != td[c.r][c.c] {
			t.Errorf("bad data\ngot:  [%d,%d] %q\nwant: %s", c.r, c.c, td[c.r][c.c], c.String())
			continue
		}
	}

	_ = widths
	// fmt.Println("---")
	// fmt.Println("Table Dump:")
	// for _, row := range td {
	// 	for c, w := range widths {
	// 		if c < len(row) {
	// 			fmt.Printf(" | %-*s", w, row[c])
	// 		} else {
	// 			fmt.Printf(" | %-*s", w, "~")
	// 		}
	// 	}
	// 	fmt.Println(" |")
	// }
	// fmt.Println("---")

}

func TestOddWhiteSpaces(t *testing.T) {
	is := is.New(t)

	testPatterns := []string{
		// `\s`,            // \s only covers [\t\n\f\r ]
		// `[[:space:]]`,   // [[:space:]] is the same as \s + \v (vertical tab)
		// `\p{Zs}`,		// does not include [\t\n\f\r]
		// `\pZ`,			// still not complete
		`[\s\pZ]`,        // \t, \n, \f and \r are not included in \pZ
		`[[:space:]\pZ]`, // \t, \n, \f and \r are not included in \pZ
	}

	// Unicode has many alternative space characters, sadly, \s in golang's
	// regexp package only matches [\t\n\f\r ]
	//
	// get around this by replacing all \p{Zs} matches with a normal space character 0x20
	//
	// see:
	//	https://unicode-explorer.com/articles/space-characters
	//	https://www.compart.com/en/unicode/category/Zs
	nastySpaceRunes := []rune{
		0x0009, // | HORIZONTAL TAB			    | not matched by \pZ
		0x000A, // | NEWLINE					| not matched by \pZ
		0x000D, // | CARRIAGE RETURN			| not matched by \pZ
		0x0020, // | SPACE                      | the regular space character as produced by pressing the space bar of your keyboard.
		0x00A0, // | NO-BREAK SPACE             | A fixed space that prevents an automatic line break at its position. Abbreviation: NBSP
		0x2000, // | EN QUAD                    | A 1 en (= 1/2 em) wide space, where 1 em is the height of the current font.
		0x2001, // | EM QUAD                    | A 1 em wide space, where 1 em is the height of the current font.
		0x2002, // | EN SPACE                   | A 1 en (= 1/2 em) wide space, where 1 em is the height of the current font.
		0x2003, // | EM SPACE                   | A 1 em wide space, where 1 em is the height of the current font.
		0x2004, // | THREE-PER-EM SPACE         | A 1/3 em wide space, where 1 em is the height of the current font. "Thick Space".
		0x2005, // | FOUR-PER-EM SPACE          | A 1/4 em wide space, where 1 em is the height of the current font. "Mid Space".
		0x2006, // | SIX-PER-EM SPACE           | A 1/6 em wide space, where 1 em is the height of the current font.
		0x2007, // | FIGURE SPACE               | A space character that is as wide as fixed-width digits. Usually used when typesetting vertically aligned numbers.
		0x2008, // | PUNCTUATION SPACE          | A space character that is as wide as a period (".").
		0x2009, // | THIN SPACE                 | A 1/6 em - 1/4 em wide space, where 1 em is the height of the current font.
		0x200A, // | HAIR SPACE                 | Narrower than the "THIN SPACE", usually the thinnest space character.
		0x202F, // | NARROW NO-BREAK SPACE      | A narrow form of a no-break space, typically the width of a "THIN SPACE". Abbreviation: NNBSP.
		0x205F, // | MEDIUM MATHEMATICAL SPACE  | A 4/18 em wide space, where 1 em is the height of the current font. Usually used when typesetting mathematical formulas.
		0x3000, // | IDEOGRAPHIC SPACE          | Regular Space Characters with Zero Width
	}
	// t.Logf("nasty runes: %#v", nastySpaceRunes)

	nastySpaceString := string(nastySpaceRunes)
	// t.Logf("nasty spaces: %q", nastySpaceString)

	spaceOnlyString := strings.Repeat(" ", len(nastySpaceRunes))

	for _, pat := range testPatterns {
		spaceFinder := regexp.MustCompile(pat)

		// try to replace all weird spaces with normal space characters (0x20)
		niceSpaceString := spaceFinder.ReplaceAllString(nastySpaceString, " ")
		is.Equal(niceSpaceString, spaceOnlyString, "all weird spaces should be replaced with normal spaces")

		// check each space character individually
		for i, r := range nastySpaceRunes {
			m := spaceFinder.FindStringIndex(string(r))
			is.NotNil(m, "%q #%d matches 0x%x", pat, i, r)
		}
	}
}
