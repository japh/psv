package psv

import (
	"bytes"
	"fmt"
	"regexp"
)

//
// Ruler represents a horizontal separator to be placed within a table.
//
// Rulers only contain the characters |, :, *, +, - and optional spaces.
//
// When a table is rendered, rulers are generated to fit the column widths of
// the table.
//
// If the input string contains 4 runes or less, it is interpreted as a format:
//
//  e.g.   '| -:'
//          |||+--  internal column separator
//          ||+---  horizonal line
//          |+----  padding
//          +-----  outer border
//
//
// If the input string is more than 4 runes, it is assumed to be
// a previously-generated ruler, and is analysed to determine
// the format.
//
// If the format of a ruler cannot be determined, the 'border'
// rune will be 0 and the original input is used.
// (non-destructive failure)
//
type Ruler struct {
	input  string // original input string, used when unparsable
	border rune   // outer vertical lines
	pad    rune   // padding on either side of column separators
	line   rune   // line characters (repeated to fill column width)
	sep    rune   // vertical separator between columns
}

var isEmptyRowRE = regexp.MustCompile(`^[\s\pZ|]*$`)
var isRulerRE = regexp.MustCompile(`^[\s\pZ|:=#*+~-]*$`)

func isRuler(input string) bool {
	switch {
	case isEmptyRowRE.MatchString(input):
		return false
	case !isRulerRE.MatchString(input):
		return false
	}
	return true
}

// NewRuler creates a new ruler based on a format or previously rendered ruler.
// Note: a returned error is really only a warning! The returned *Ruler is still valid and usable!
func NewRuler(input string) (*Ruler, error) {
	r := &Ruler{}
	err := r.decode(input)
	if err != nil {
		return r, err
	}
	return r, nil
}

func (r *Ruler) format() string {
	if r.border == 0 {
		return ""
	}

	b := bytes.Buffer{}
	b.WriteRune(r.border)
	b.WriteRune(r.pad)
	b.WriteRune(r.line)
	b.WriteRune(r.sep)

	return b.String()
}

func (r *Ruler) decode(input string) error {

	// reset
	r.input = input
	r.border = 0
	r.pad = 0
	r.line = 0
	r.sep = 0

	format := []rune(input)

	// assume that the 1st 4 characters are
	//      border
	//      pad
	//      line
	//      sep
	for p, ch := range format {
		switch p {
		case 0:
			r.border = ch
		case 1:
			r.pad = ch
		case 2:
			r.line = ch
		case 3:
			r.sep = ch
		default:
			break
		}
	}

	// special case: '|' is never used for horozontal lines!
	if r.pad == '|' {
		r.pad = ' '
		r.sep = '|'
	}
	if r.line == '|' {
		r.line = ' '
		r.sep = '|'
	}

	//
	// > 4 runes? => pre-formatted ruler - not a format
	//
	//  1. only border and padding are valid
	//  2. the last two characters should be padding and border
	//  3. the remaining unique runes should then match one of
	//     the following patterns:
	//
	//      <empty>         1 column with width 0
	//      line            1 column with width >0
	//      pad, sep        multiple columns, all with width 0
	//      pad, sep, line  multiple columns, first with width 0
	//      line, pad, sep  multiple columns, first with width >0
	//
	//     BUT, if any of the characters are identical (e.g.
	//     pad and line) we can also get the following, 2-run
	//     combinations:
	//
	//      (pad/sep), line
	//      pad, (sep/line)
	//      (line/pad), sep
	//      line, (pad/sep)
	//
	//     And finally, a single rune must be interpreted as:
	//
	//      (line/pad/sep)  same rune used for all
	//
	if len(format) > 4 {

		// 1. invalidate line and separator runes
		r.line = 0
		r.sep = 0

		// 2. strip (ignore) the last two runes if they match
		// the border and padding runes.
		end := len(format) - 1
		p := 0
		for p < 2 {
			if format[p] != format[end-p] {
				break
			}
			p++
		}
		end = len(format) - p

		// get an ordered list of unique runes between the two,
		// padded, outer borders
		dup := make(map[rune]struct{}, len(format))
		unique := make([]rune, 0, 3)
		for _, ch := range format[2:end] {
			if _, d := dup[ch]; d {
				continue
			}
			dup[ch] = struct{}{}
			unique = append(unique, ch)
		}

		switch len(unique) {
		case 0: // use defaults
		case 1: // use defaults
			r.line = unique[0]
			if r.line == r.pad {
				r.sep = r.line
			}
		case 2:
			// nasty: just try the 4 possibilities (most
			// probable first) to see which one actually
			// produces a pattern which matches the ruler we
			// were given.
			attempts := [][]rune{
				{unique[0], unique[1]},
				{unique[0], unique[0]},
				{unique[1], unique[0]},
				{unique[1], unique[1]},
			}
			for _, a := range attempts {
				if r.isPlausible(a) {
					r.line = a[0]
					r.sep = a[1]
					break
				}
			}
			if r.line == 0 {
				r.border = 0
				return fmt.Errorf("%q does not have plausibly spaced columns", truncate(r.input, 10))
			}
		case 3:
			switch {
			case unique[1] == r.pad:
				// most likely, first column has width >0
				r.line = unique[0]
				r.sep = unique[2]
			case unique[0] == r.pad:
				// empty first column, separator appears before line
				r.sep = unique[1]
				r.line = unique[2]
			default:
				// something is wrong with this pattern, give up
				r.border = 0
				return fmt.Errorf("%q has mismatching format characters", truncate(r.input, 10))
			}
		default:
			// more than 3 unique characters, give up
			r.border = 0
			return fmt.Errorf("%q has too many unique characters to form a ruler (max 4)", truncate(r.input, 10))
		}
	}

	// fill in defaults if neccessary
	if r.border == 0 {
		r.border = '-'
	}
	if r.pad == 0 {
		if r.border != '|' {
			r.pad = r.border
		} else {
			r.pad = ' '
		}
	}
	if r.line == 0 {
		r.line = r.pad
	}
	if r.sep == 0 {
		r.sep = r.border
	}

	return nil
}

func truncate(text string, max int) string {
	if max < 6 {
		max = 6
	}
	if len(text) > max {
		return text[:max-3] + "..."
	}
	return text
}

func (r *Ruler) isPlausible(a []rune) bool {
	pattern := fmt.Sprintf(`^%s%s%s*?(%s%s%s%s*?)*%s%s$`,
		regexp.QuoteMeta(string(r.border)),
		regexp.QuoteMeta(string(r.pad)),
		regexp.QuoteMeta(string(a[0])), // line
		regexp.QuoteMeta(string(r.pad)),
		regexp.QuoteMeta(string(a[1])), // separator
		regexp.QuoteMeta(string(r.pad)),
		regexp.QuoteMeta(string(a[0])), // line
		regexp.QuoteMeta(string(r.pad)),
		regexp.QuoteMeta(string(r.border)),
	)
	attemptRE := regexp.MustCompile(pattern)
	return attemptRE.MatchString(r.input)
}

// Encode generates a string using the ruler's format to match the column widths provided in 'widths'
func (r *Ruler) Encode(widths []int) string {
	if r.border == 0 {
		return r.input
	}

	end := len(widths) - 1
	b := bytes.Buffer{}
	b.WriteRune(r.border)
	for c, w := range widths {
		b.WriteRune(r.pad)
		for i := 0; i < w; i++ {
			b.WriteRune(r.line)
		}
		b.WriteRune(r.pad)
		if c != end {
			b.WriteRune(r.sep)
		} else {
			b.WriteRune(r.border)
		}
	}

	return b.String()
}
