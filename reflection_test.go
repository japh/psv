//go:build ignore

package psv_test

import (
	"fmt"
	"reflect"
	"testing"

	_ "codeberg.org/japh/psv"
)

var kindName = map[reflect.Kind]string{
	reflect.Invalid:       "Invalid",
	reflect.Bool:          "Bool",
	reflect.Int:           "Int",
	reflect.Int8:          "Int8",
	reflect.Int16:         "Int16",
	reflect.Int32:         "Int32",
	reflect.Int64:         "Int64",
	reflect.Uint:          "Uint",
	reflect.Uint8:         "Uint8",
	reflect.Uint16:        "Uint16",
	reflect.Uint32:        "Uint32",
	reflect.Uint64:        "Uint64",
	reflect.Uintptr:       "Uintptr",
	reflect.Float32:       "Float32",
	reflect.Float64:       "Float64",
	reflect.Complex64:     "Complex64",
	reflect.Complex128:    "Complex128",
	reflect.Array:         "Array",
	reflect.Chan:          "Chan",
	reflect.Func:          "Func",
	reflect.Interface:     "Interface",
	reflect.Map:           "Map",
	reflect.Ptr:           "Ptr",
	reflect.Slice:         "Slice",
	reflect.String:        "String",
	reflect.Struct:        "Struct",
	reflect.UnsafePointer: "UnsafePointer",
}

func dump(v interface{}) {
	fmt.Println("---")
	fmt.Printf("dumping %T %v\n", v, v)
	rt, rv := reflect.TypeOf(v), reflect.ValueOf(v)
	fmt.Printf("    type:  %v\n", rt)
	fmt.Printf("    kind:  %v\n", kindName[rt.Kind()])
	fmt.Printf("    value: %#+v\n", rv)

	if rt.Kind() == reflect.Struct {
		for i := 0; i < rt.NumField(); i++ {
			field := rt.Field(i)
			if alias, ok := field.Tag.Lookup("tbl"); ok {
				if alias == "" {
					fmt.Println("(blank)")
				} else {
					fmt.Println(alias)
				}
			} else {
				fmt.Printf("(%s)\n", field.Name)
			}
		}

	}

	if rt.Kind() == reflect.Slice || rt.Kind() == reflect.Array {
		// fmt.Println(psv.TableFromSlice(v))
	}
}

type Name string

type TokenID int

type Token struct {
	ID     TokenID `tbl:"id" json:"tid"`
	Pos    int     `tbl:"position"`
	secret string
	Name
}

func TestReflection(t *testing.T) {
	// basic types
	dump(1)

	i := 1
	dump(i)
	dump(&i)

	dump("hello")

	s := "world"
	dump(s)
	dump(&s)

	// typed basic types
	var id TokenID = 5
	dump(id)
	dump(&id)
	var nm Name = "Foo"
	dump(nm)
	dump(&nm)

	tok := Token{5, 6, "gummibears", "Joe"}
	dump(tok)
	dump(&tok)

	toka := [...]Token{
		{5, 6, "gummibears", "Joe"},
	}
	dump(toka)
	dump(&toka)

	toks := []Token{
		{5, 6, "gummibears", "Joe"},
	}
	dump(toks)
	dump(&toks)
}
