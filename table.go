package psv

import (
	"bufio"
	"bytes"
	"strings"
)

// Table is a structure used to encapsulate a table of string data, which can
// be re-rendered with correct indentation and column alignment.
// The exported fields of Table may be used to customize the rendered result.
type Table struct {
	Options               // configuration options
	Data      [][]string  // 2-Dimensional array of string cells indexed by [row][column]
	TextLines []*TextLine // an array of decorative (non-table) text lines to add to the table
	sections  []*Section  // an array of sections
	firstRow  int         // first row of table data
	lastRow   int         // last row of table data
	*Indenter             // indent matcher and producer
}

// Options is used by Table to store user specified configuration settings
type Options struct {
	IndentPattern string   // user-provided indent to use
	IndentIsFinal bool     // flag to force the use of Options.Indent (required to force the use of "")
	Squash        bool     // squash multiple blank lines into a single blank line when encoding
	Sort          bool     // sort data rows, see also: SortColumns and SortSections
	SortLanguage  string   // the BCP 47 locale to use for collation rules
	SortColumns   []string // list of column names or numbers to use for sorting
	SortSections  []string // list of section names or numbers to use for sorting
}

// TextLine specifies a single non-table text line to be positioned at a
// specific point in the generated table string.
type TextLine struct {
	Line   int    // 1-based number of this line's position in the resulting table
	Indent string // only used to re-construct original indentation outside a table's bounds
	Text   string // un-indented text to be inserted into the table, may be ""
	Ruler  string // ruler specification, see Ruler type
}

func (d *TextLine) Clone() *TextLine {
	return &TextLine{
		Line:   d.Line,
		Indent: d.Indent,
		Text:   d.Text,
		Ruler:  d.Ruler,
	}
}

// Section represents a set of consecutive data rows.
//
// Any decorative text within a table causes the table to be split into
// multiple sections.
//
// By default, each section may have a different set of columns, but when the
// MergeSections (-m or -merge-sections) option is used, then the first data
// row found determines the number of columns in the table
type Section struct {
	name    string   // optional section name
	start   int      // index into Data of first row in this section
	end     int      // index into Data or last row (+1) in this section
	columns []string // reference to the most recent header row
}

// Name returns the name of a section.
//
// Sections are named by a single, non-empty line directly before a
// data row. Rulers are ignored completely.
//
// e.g. below is a table with 2 sections names "Section 1" and
//		"Section 2"
//
//		| heading |
//	Section 1
//		+ ------- +
//		| data    |
//		| data    |
//		+ ------- +
//	Section 2
//		| data    |
//		+ ------- +
//		| data    |
//		+ ------- +
func (s *Section) Name() string {
	return s.name
}

// ColumnNames returns the []string from the first row of data
// after a section header.
func (s *Section) ColumnNames() []string {
	return s.columns
}

// RowCount returns the number of data rows within this section.
func (s *Section) RowCount() int {
	return s.end - s.start
}

// RowSpan returns the indexes of the first and last data rows of
// the section, in the table data table.
func (s *Section) RowSpan() (int, int) {
	return s.start, s.end
}

//
// New creates a new, empty table.
// Use its public fields to setup the data to be printed.
//
// See also: encode_test.go
//
func New() *Table {
	t := &Table{}
	t.Data = [][]string{}
	t.TextLines = []*TextLine{}
	t.Indenter = NewIndenter()
	return t
}

// NewTable creates a new, empty table.
// Use its public fields to setup the data to be printed.
//
// See also: encode_test.go
//
// Deprecated: prefer psv.New()
//
func NewTable() *Table { return New() }

// Clone provides a deep-copy of an existing Table
func (tbl *Table) Clone() *Table {
	clone := New()

	// duplicate table data
	clone.Data = make([][]string, 0, len(tbl.Data))
	for r := range tbl.Data {
		clonedRow := make([]string, 0, len(tbl.Data[r]))
		for c := range tbl.Data[r] {
			clonedRow = append(clonedRow, tbl.Data[r][c])
		}
		clone.Data = append(clone.Data, clonedRow)
	}

	// duplicate text lines
	clone.TextLines = make([]*TextLine, 0, len(tbl.TextLines))
	for d := range tbl.TextLines {
		clone.TextLines = append(clone.TextLines, tbl.TextLines[d].Clone())
	}

	// duplicate indenter
	clone.Indenter = tbl.Indenter.Clone()

	return clone
}

// TableFromString Creates a new table from a string containing a psv table.
// This is the recommended way to read in-situ tables.
// e.g.
//      tbl, err := psv.FromString(`
//                                 | 1 | one |
//                                 | 2 | two |
//                                 :         :
//                                 `)
func TableFromString(input string) (*Table, error) {
	tbl := NewTable()
	err := tbl.DecodeString(input)
	if err != nil {
		return tbl, err
	}
	// tbl.analyseTableStructure()
	return tbl, nil
}

// SetData replaces the table's data with a new, 2 dimensional
// slice of strings.
// Note that existing TextLines are left unchanged.
func (tbl *Table) SetData(data [][]string) {
	tbl.Data = data
}

// AppendDataRow adds a single row of data to the end of the table
// A row is just a []string
// Rows do not have to have a specific number of columns, all rows are
// normalised to a consistent width before encoding
func (tbl *Table) AppendDataRow(row []string) {
	tbl.Data = append(tbl.Data, row)
}

// AppendText adds any number of decorative text lines to the end
// of the table.
func (tbl *Table) AppendText(textLines ...string) {
	line := 1 + len(tbl.Data) + len(tbl.TextLines)
	for offset, text := range textLines {
		d := &TextLine{
			Line: line + offset,
			Text: text,
		}
		tbl.TextLines = append(tbl.TextLines, d)
	}
}

// AppendRuler add a ruler line to the end of the table.
//
// Rulers are specified by an up-to 4 character string, with 1 character each
// for the outer borders, column padding, horizonal lines and internal column
// separator.
//
// e.g.:
//      //               ,-----  outer border
//      //               |,----  padding
//      //               ||,---  horizonal line
//      //               |||,--  internal column separator
//      //               ||||
//      tbl.AppendRuler("| -:")
//
func (tbl *Table) AppendRuler(ruler string) {
	d := &TextLine{
		Line:  1 + len(tbl.Data) + len(tbl.TextLines),
		Ruler: ruler,
	}
	tbl.TextLines = append(tbl.TextLines, d)
}

// DecodeString extracts data from a block of text containing a PSV
// table
//
// See also: TableFromString to create a new table object directly
// from a string
func (tbl *Table) DecodeString(input string) error {

	r := strings.NewReader(input)
	s := bufio.NewScanner(r)
	err := tbl.Read(s)
	if err != nil {
		return err
	}

	tbl.analyseTableStructure()

	return nil
}

// Encode formats a Table struct into a multi-line string
//
// Table.Data is converted into a formatted table
// Table.TextLines are interspersed and indented according to the formatting
// rules.
func (tbl *Table) Encode() string {
	b := &bytes.Buffer{}
	tbl.Write(b)
	return b.String()
}

func (tbl *Table) Sort() {
	if !tbl.Options.Sort {
		return
	}
	sorter := tbl.NewSorter()
	sorter.Language(tbl.Options.SortLanguage)
	sorter.InSections(tbl.Options.SortSections)
	sorter.ByColumns(tbl.Options.SortColumns)
	sorter.Sort()
}

// DataRows returns the [][]string of data in the table.
// The rows returned are guaranteed to all have the same number of columns
//
//	tbl := psv.TableFromString(...).DataRows()
//		for _, row := range tbl {
//			for _, cell := range row {
//				...
//			}
//		}
//	}
//
func (tbl *Table) DataRows() [][]string {
	return tbl.Data
}

func (tbl *Table) Sections() []*Section {
	tbl.analyseTableStructure()
	return tbl.sections
}

func (tbl *Table) analyseTableStructure() {
	var section *Section // the section being scanned
	var columns []string // the column names from the most recently found header
	var name string      // the most recently discovered section name
	line := 0            // current table line (textLine+dataRow)
	textLine := 0        // current decorative text line index
	nameStart := -1      // first line of most recent decorative text block
	nameEnd := 0         // last line of most recent decorative text block
	dataRow := 0         // current data row index

	tbl.sections = []*Section{}

	for textLine < len(tbl.TextLines) || dataRow < len(tbl.Data) {
		line++

		if textLine < len(tbl.TextLines) {
			deco := tbl.TextLines[textLine]
			if line >= deco.Line {

				// close the previous section
				if section != nil {
					if deco.Ruler != "" && section.RowCount() == 1 {
						section.columns = tbl.Data[section.start]
						columns = section.columns
					}
					tbl.sections = append(tbl.sections, section)
					section = nil
				}

				switch {
				case deco.Text == "" && deco.Ruler == "":
					nameStart = -1
				case deco.Text != "":
					if nameStart == -1 {
						nameStart = textLine
					}
					nameEnd = textLine
				case deco.Ruler != "":
					if nameStart >= 0 && nameEnd-nameStart == 0 {
						name = tbl.TextLines[nameStart].Text
						nameStart = -1
					}
				}

				textLine++
				continue
			}
		}

		if section == nil {
			// if last decorative text was just 1 line, use it as the section name
			if nameStart >= 0 && nameEnd-nameStart == 0 {
				name = tbl.TextLines[nameStart].Text
				nameStart = -1
			}
			section = &Section{
				name:    stripIndentRE.ReplaceAllString(name, ""),
				start:   dataRow,
				columns: columns,
			}
		}

		dataRow++
		section.end = dataRow
	}

	if section != nil {
		// last section cannot be a header
		tbl.sections = append(tbl.sections, section)
	}
}

func (tbl *Table) ColumnNames() []string {
	for _, s := range tbl.sections {
		return s.ColumnNames()
	}
	return nil
}
