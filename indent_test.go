package psv_test

import (
	"fmt"

	"codeberg.org/japh/psv"
)

func ExampleIndenter() {

	inputLines := []string{
		``,
		`hello`,
		`    hello`,
		`// hello`,
		`// // hello`,
		`    //    hello`,
	}

	patterns := []struct {
		pattern string
		isFinal bool
	}{
		{pattern: ""},
		{pattern: ``, isFinal: true},
		{pattern: `0`},
		{pattern: `-1`},
		{pattern: `-0`},
		{pattern: `+1`},
		{pattern: `1`},
		{pattern: `63`},
		{pattern: `64`},
		{pattern: `  `},
		{pattern: `//`},
		{pattern: ` //`},
		{pattern: ` // `},
		{pattern: `> >`},
		{pattern: ` > >`},
		{pattern: ` > > `},
	}

PATTERNS:
	for _, p := range patterns {
		for _, l := range inputLines {
			i := psv.NewIndenter()

			fmt.Printf("indent option:    %q\n", p.pattern)

			if p.pattern != "" || p.isFinal {
				err := i.SetIndent(p.pattern)
				if err != nil {
					fmt.Printf("failed to set indent: %q\n\n", err)
					continue PATTERNS
				}
			}

			fmt.Printf("indenter:         %s\n", i.String())

			found := i.FindIndent(l)
			i.FinalizeIndent()

			fmt.Printf("input line:       %q\n", l)
			fmt.Printf("detected  indent: %q\n", found)
			fmt.Printf("finalised indent: %q\n", i.Indent()+l[len(found):])
			fmt.Println()
		}
		fmt.Println("----")
	}

	// output:
	// indent option:    ""
	// indenter:         "" (default)
	// input line:       ""
	// detected  indent: ""
	// finalised indent: ""
	//
	// indent option:    ""
	// indenter:         "" (default)
	// input line:       "hello"
	// detected  indent: ""
	// finalised indent: "hello"
	//
	// indent option:    ""
	// indenter:         "" (default)
	// input line:       "    hello"
	// detected  indent: "    "
	// finalised indent: "    hello"
	//
	// indent option:    ""
	// indenter:         "" (default)
	// input line:       "// hello"
	// detected  indent: ""
	// finalised indent: "// hello"
	//
	// indent option:    ""
	// indenter:         "" (default)
	// input line:       "// // hello"
	// detected  indent: ""
	// finalised indent: "// // hello"
	//
	// indent option:    ""
	// indenter:         "" (default)
	// input line:       "    //    hello"
	// detected  indent: "    "
	// finalised indent: "    //    hello"
	//
	// ----
	// indent option:    ""
	// indenter:         "" (finalised)
	// input line:       ""
	// detected  indent: ""
	// finalised indent: ""
	//
	// indent option:    ""
	// indenter:         "" (finalised)
	// input line:       "hello"
	// detected  indent: ""
	// finalised indent: "hello"
	//
	// indent option:    ""
	// indenter:         "" (finalised)
	// input line:       "    hello"
	// detected  indent: "    "
	// finalised indent: "hello"
	//
	// indent option:    ""
	// indenter:         "" (finalised)
	// input line:       "// hello"
	// detected  indent: ""
	// finalised indent: "// hello"
	//
	// indent option:    ""
	// indenter:         "" (finalised)
	// input line:       "// // hello"
	// detected  indent: ""
	// finalised indent: "// // hello"
	//
	// indent option:    ""
	// indenter:         "" (finalised)
	// input line:       "    //    hello"
	// detected  indent: "    "
	// finalised indent: "//    hello"
	//
	// ----
	// indent option:    "0"
	// indenter:         "" (finalised)
	// input line:       ""
	// detected  indent: ""
	// finalised indent: ""
	//
	// indent option:    "0"
	// indenter:         "" (finalised)
	// input line:       "hello"
	// detected  indent: ""
	// finalised indent: "hello"
	//
	// indent option:    "0"
	// indenter:         "" (finalised)
	// input line:       "    hello"
	// detected  indent: "    "
	// finalised indent: "hello"
	//
	// indent option:    "0"
	// indenter:         "" (finalised)
	// input line:       "// hello"
	// detected  indent: ""
	// finalised indent: "// hello"
	//
	// indent option:    "0"
	// indenter:         "" (finalised)
	// input line:       "// // hello"
	// detected  indent: ""
	// finalised indent: "// // hello"
	//
	// indent option:    "0"
	// indenter:         "" (finalised)
	// input line:       "    //    hello"
	// detected  indent: "    "
	// finalised indent: "//    hello"
	//
	// ----
	// indent option:    "-1"
	// failed to set indent: "indent must be an unsigned integer <64 or non-numeric string"
	//
	// indent option:    "-0"
	// failed to set indent: "indent must be an unsigned integer <64 or non-numeric string"
	//
	// indent option:    "+1"
	// failed to set indent: "indent must be an unsigned integer <64 or non-numeric string"
	//
	// indent option:    "1"
	// indenter:         " " (finalised)
	// input line:       ""
	// detected  indent: ""
	// finalised indent: " "
	//
	// indent option:    "1"
	// indenter:         " " (finalised)
	// input line:       "hello"
	// detected  indent: ""
	// finalised indent: " hello"
	//
	// indent option:    "1"
	// indenter:         " " (finalised)
	// input line:       "    hello"
	// detected  indent: "    "
	// finalised indent: " hello"
	//
	// indent option:    "1"
	// indenter:         " " (finalised)
	// input line:       "// hello"
	// detected  indent: ""
	// finalised indent: " // hello"
	//
	// indent option:    "1"
	// indenter:         " " (finalised)
	// input line:       "// // hello"
	// detected  indent: ""
	// finalised indent: " // // hello"
	//
	// indent option:    "1"
	// indenter:         " " (finalised)
	// input line:       "    //    hello"
	// detected  indent: "    "
	// finalised indent: " //    hello"
	//
	// ----
	// indent option:    "63"
	// indenter:         "                                                               " (finalised)
	// input line:       ""
	// detected  indent: ""
	// finalised indent: "                                                               "
	//
	// indent option:    "63"
	// indenter:         "                                                               " (finalised)
	// input line:       "hello"
	// detected  indent: ""
	// finalised indent: "                                                               hello"
	//
	// indent option:    "63"
	// indenter:         "                                                               " (finalised)
	// input line:       "    hello"
	// detected  indent: "    "
	// finalised indent: "                                                               hello"
	//
	// indent option:    "63"
	// indenter:         "                                                               " (finalised)
	// input line:       "// hello"
	// detected  indent: ""
	// finalised indent: "                                                               // hello"
	//
	// indent option:    "63"
	// indenter:         "                                                               " (finalised)
	// input line:       "// // hello"
	// detected  indent: ""
	// finalised indent: "                                                               // // hello"
	//
	// indent option:    "63"
	// indenter:         "                                                               " (finalised)
	// input line:       "    //    hello"
	// detected  indent: "    "
	// finalised indent: "                                                               //    hello"
	//
	// ----
	// indent option:    "64"
	// failed to set indent: "indent must be an unsigned integer <64 or non-numeric string"
	//
	// indent option:    "  "
	// indenter:         "  " (finalised)
	// input line:       ""
	// detected  indent: ""
	// finalised indent: "  "
	//
	// indent option:    "  "
	// indenter:         "  " (finalised)
	// input line:       "hello"
	// detected  indent: ""
	// finalised indent: "  hello"
	//
	// indent option:    "  "
	// indenter:         "  " (finalised)
	// input line:       "    hello"
	// detected  indent: "    "
	// finalised indent: "  hello"
	//
	// indent option:    "  "
	// indenter:         "  " (finalised)
	// input line:       "// hello"
	// detected  indent: ""
	// finalised indent: "  // hello"
	//
	// indent option:    "  "
	// indenter:         "  " (finalised)
	// input line:       "// // hello"
	// detected  indent: ""
	// finalised indent: "  // // hello"
	//
	// indent option:    "  "
	// indenter:         "  " (finalised)
	// input line:       "    //    hello"
	// detected  indent: "    "
	// finalised indent: "  //    hello"
	//
	// ----
	// indent option:    "//"
	// indenter:         "//" (default)
	// input line:       ""
	// detected  indent: ""
	// finalised indent: "// "
	//
	// indent option:    "//"
	// indenter:         "//" (default)
	// input line:       "hello"
	// detected  indent: ""
	// finalised indent: "// hello"
	//
	// indent option:    "//"
	// indenter:         "//" (default)
	// input line:       "    hello"
	// detected  indent: "    "
	// finalised indent: "//    hello"
	//
	// indent option:    "//"
	// indenter:         "//" (default)
	// input line:       "// hello"
	// detected  indent: "// "
	// finalised indent: "// hello"
	//
	// indent option:    "//"
	// indenter:         "//" (default)
	// input line:       "// // hello"
	// detected  indent: "// // "
	// finalised indent: "// // hello"
	//
	// indent option:    "//"
	// indenter:         "//" (default)
	// input line:       "    //    hello"
	// detected  indent: "    //    "
	// finalised indent: "    //    hello"
	//
	// ----
	// indent option:    " //"
	// indenter:         " //" (finalised)
	// input line:       ""
	// detected  indent: ""
	// finalised indent: " //"
	//
	// indent option:    " //"
	// indenter:         " //" (finalised)
	// input line:       "hello"
	// detected  indent: ""
	// finalised indent: " //hello"
	//
	// indent option:    " //"
	// indenter:         " //" (finalised)
	// input line:       "    hello"
	// detected  indent: "    "
	// finalised indent: " //hello"
	//
	// indent option:    " //"
	// indenter:         " //" (finalised)
	// input line:       "// hello"
	// detected  indent: "// "
	// finalised indent: " //hello"
	//
	// indent option:    " //"
	// indenter:         " //" (finalised)
	// input line:       "// // hello"
	// detected  indent: "// // "
	// finalised indent: " //hello"
	//
	// indent option:    " //"
	// indenter:         " //" (finalised)
	// input line:       "    //    hello"
	// detected  indent: "    //    "
	// finalised indent: " //hello"
	//
	// ----
	// indent option:    " // "
	// indenter:         " // " (finalised)
	// input line:       ""
	// detected  indent: ""
	// finalised indent: " // "
	//
	// indent option:    " // "
	// indenter:         " // " (finalised)
	// input line:       "hello"
	// detected  indent: ""
	// finalised indent: " // hello"
	//
	// indent option:    " // "
	// indenter:         " // " (finalised)
	// input line:       "    hello"
	// detected  indent: "    "
	// finalised indent: " // hello"
	//
	// indent option:    " // "
	// indenter:         " // " (finalised)
	// input line:       "// hello"
	// detected  indent: "// "
	// finalised indent: " // hello"
	//
	// indent option:    " // "
	// indenter:         " // " (finalised)
	// input line:       "// // hello"
	// detected  indent: "// // "
	// finalised indent: " // hello"
	//
	// indent option:    " // "
	// indenter:         " // " (finalised)
	// input line:       "    //    hello"
	// detected  indent: "    //    "
	// finalised indent: " // hello"
	//
	// ----
	// indent option:    "> >"
	// indenter:         "> >" (default)
	// input line:       ""
	// detected  indent: ""
	// finalised indent: "> > "
	//
	// indent option:    "> >"
	// indenter:         "> >" (default)
	// input line:       "hello"
	// detected  indent: ""
	// finalised indent: "> > hello"
	//
	// indent option:    "> >"
	// indenter:         "> >" (default)
	// input line:       "    hello"
	// detected  indent: "    "
	// finalised indent: "> >    hello"
	//
	// indent option:    "> >"
	// indenter:         "> >" (default)
	// input line:       "// hello"
	// detected  indent: ""
	// finalised indent: "> > // hello"
	//
	// indent option:    "> >"
	// indenter:         "> >" (default)
	// input line:       "// // hello"
	// detected  indent: ""
	// finalised indent: "> > // // hello"
	//
	// indent option:    "> >"
	// indenter:         "> >" (default)
	// input line:       "    //    hello"
	// detected  indent: "    "
	// finalised indent: "> >    //    hello"
	//
	// ----
	// indent option:    " > >"
	// indenter:         " > >" (finalised)
	// input line:       ""
	// detected  indent: ""
	// finalised indent: " > >"
	//
	// indent option:    " > >"
	// indenter:         " > >" (finalised)
	// input line:       "hello"
	// detected  indent: ""
	// finalised indent: " > >hello"
	//
	// indent option:    " > >"
	// indenter:         " > >" (finalised)
	// input line:       "    hello"
	// detected  indent: "    "
	// finalised indent: " > >hello"
	//
	// indent option:    " > >"
	// indenter:         " > >" (finalised)
	// input line:       "// hello"
	// detected  indent: ""
	// finalised indent: " > >// hello"
	//
	// indent option:    " > >"
	// indenter:         " > >" (finalised)
	// input line:       "// // hello"
	// detected  indent: ""
	// finalised indent: " > >// // hello"
	//
	// indent option:    " > >"
	// indenter:         " > >" (finalised)
	// input line:       "    //    hello"
	// detected  indent: "    "
	// finalised indent: " > >//    hello"
	//
	// ----
	// indent option:    " > > "
	// indenter:         " > > " (finalised)
	// input line:       ""
	// detected  indent: ""
	// finalised indent: " > > "
	//
	// indent option:    " > > "
	// indenter:         " > > " (finalised)
	// input line:       "hello"
	// detected  indent: ""
	// finalised indent: " > > hello"
	//
	// indent option:    " > > "
	// indenter:         " > > " (finalised)
	// input line:       "    hello"
	// detected  indent: "    "
	// finalised indent: " > > hello"
	//
	// indent option:    " > > "
	// indenter:         " > > " (finalised)
	// input line:       "// hello"
	// detected  indent: ""
	// finalised indent: " > > // hello"
	//
	// indent option:    " > > "
	// indenter:         " > > " (finalised)
	// input line:       "// // hello"
	// detected  indent: ""
	// finalised indent: " > > // // hello"
	//
	// indent option:    " > > "
	// indenter:         " > > " (finalised)
	// input line:       "    //    hello"
	// detected  indent: "    "
	// finalised indent: " > > //    hello"
	//
	// ----

}

func ExampleIndenter_SetIndent_remove_indents() {

	testCases := []string{
		"",  // explicitly request an empty-string indent
		"0", // explicitly set the indent width to 0
	}

	for _, testIndent := range testCases {
		tbl := psv.NewTable()
		tbl.SetIndent(testIndent)
		tbl.DecodeString(``)
	}

	// output:
	//

}
