package psv

type textQueue struct {
	q []*TextLine
}

func (q *textQueue) next() *TextLine {
	if len(q.q) == 0 {
		return nil
	}
	l := q.q[0]
	q.q = q.q[1:]
	return l
}
