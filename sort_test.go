package psv_test

import (
	"fmt"
	"strconv"
	"strings"
	"testing"

	"codeberg.org/japh/is"
	"codeberg.org/japh/psv"
)

func TestEmptyTableSorting(t *testing.T) {
	testTable := psv.New()
	testCases := sortingTestCasesFromTable(`
		| name                                | sort  | sections | columns |
		| ----------------------------------- | ----- | -------- | ------- |
		| no sort                             | false |          |         |
		| default sort                        |       |          |         |
		| sort non-existent columns by index  |       |          | 1       |
		| sort non-existent columns by name   |       |          | foo     |
		| sort non-existent sections by index |       | 1        |         |
		| sort non-existent sections by name  |       | foo      |         |
		`)

	runSortingTestCases(t, testTable, testCases)
}

func TestSingleSectionSorting(t *testing.T) {

	testTable, _ := psv.TableFromString(`
        | 0 | b | 3  | partial
        | 1 | D
        | 2 | E | 5
        | 3 | a | 4  | unequal
        | 4 | c | 20
        | 5 | C | 10 | row | lengths
        | 6 | e | 5
        | 7 | d | 7
        `)

	testCases := sortingTestCasesFromTable(`
		| name                                  | skip               | sort  | columns | exp-col | exp-rows        | comments                                                            |
		| ------------------------------------- | ------------------ | ----- | ------- | ------- | --------------- | ------------------------------------------------------------------- |
		| no sort                               |                    | false |         |         | 0 1 2 3 4 5 6 7 | unchanged                                                           |
		| default lexographic sort              | default is numeric |       |         |         | 0 1 2 3 4 5 6 7 | lexographically sorted left to right                                |
		| default numeric sort                  |                    |       |         |         | 0 1 2 3 4 5 6 7 | numerically sorted from left to right                               |
		| sort only when asked to               |                    | false | 2       |         | 0 1 2 3 4 5 6 7 | don't sort just because columns or sections have been defined       |
		| reverse default sort                  |                    |       | ~       |         | 7 6 5 4 3 2 1 0 |                                                                     |
		| reverse reverse default sort          |                    |       | ~~      |         | 0 1 2 3 4 5 6 7 |                                                                     |
		| indexed column sort                   |                    |       | 2       |         | 3 0 4 5 7 1 6 2 | language.Und sorts lower-case before uppercase                      |
		| indexed column sort                   |                    |       | 2       | 2       | a b c C d D e E | language.Und sorts lower-case before uppercase                      |
		| reverse column sort                   |                    |       | ~2      |         | 2 6 1 7 5 4 0 3 |                                                                     |
		| third column default lexographic sort | default is numeric |       | 3       |         | 1 5 4 0 3 2 6 7 | d == "" - not possible in a space-sep. list                         |
		| third column default numeric sort     |                    |       | 3       |         | 1 0 3 2 6 7 5 4 | d == "" - not possible in a space-sep. list                         |
		| lexographic sort                      |                    |       | @3      |         | 1 5 4 0 3 2 6 7 |                                                                     |
		| numeric sort                          |                    |       | #3      |         | 1 0 3 2 6 7 5 4 |                                                                     |
		| reverse numeric sort                  |                    |       | ~#3     |         | 4 5 7 2 6 3 0 1 | known issue: stable sort does NOT reverse order of identical items! |
		| numeric reverse sort                  |                    |       | #~3     |         | 4 5 7 2 6 3 0 1 | # and ~ flags can be provided in any order                          |
		| reverse reverse column sort           |                    |       | ~ #~3   |         | 1 0 3 2 6 7 5 4 | same as #3                                                          |
		| partial column sort                   |                    |       | 4 2     |         | 4 7 1 6 2 0 5 3 | empty and missing cells are treated as ""                           |
		| non-existent column sort              |                    |       | 9       |         | 0 1 2 3 4 5 6 7 |                                                                     |
		`)

	runSortingTestCases(t, testTable, testCases)
}

func TestCollationStyles(t *testing.T) {

	testTable, _ := psv.TableFromString(`
		| 0 | ticket #5000
		| 1 | ticket #0005
		| 2 | ticket #5
		| 3 | 2
		| 4 | 10
		| 5 | 5km/h
		| 6 | 20km/h
        `)

	testCases := sortingTestCasesFromTable(`
		| wip | name                            | skip               | columns | exp-rows      | comments                                                                              |
		| --- | ------------------------------- | ------------------ | ------- | ------------- | ------------------------------------------------------------------------------------- |
		|     | default lexographic order       | default is numeric | 2       | 4 3 6 5 1 2 0 |                                                                                       |
		|     | default numeric order           |                    | 2       | 3 5 4 6 1 2 0 |                                                                                       |
		|     | lexographic order               |                    | @2      | 4 3 6 5 1 2 0 |                                                                                       |
		|     | numeric order                   |                    | #2      | 3 5 4 6 1 2 0 |                                                                                       |
		|     | reverse lexographic order       |                    | ~@2     | 0 2 1 5 6 3 4 |                                                                                       |
		|     | reverse numeric order           |                    | ~#2     | 0 1 2 6 4 5 3 |                                                                                       |
		|     | lexographic reverse order       |                    | @~2     | 0 2 1 5 6 3 4 |                                                                                       |
		|     | numeric reverse order           |                    | #~2     | 0 1 2 6 4 5 3 | ticket #5 and ticket #0005 are 'numerically identical', rows 1 and 2 are not reversed |
		|     | multiple flags (last flag wins) |                    | #~@~#2  | 3 5 4 6 1 2 0 | numeric                                                                               |
		|     | multiple flags (last flag wins) |                    | @~#~@2  | 4 3 6 5 1 2 0 | lexographic                                                                           |
		`)

	runSortingTestCases(t, testTable, testCases)
}

func TestNamedColumnSorting(t *testing.T) {

	testTable, _ := psv.TableFromString(`
        +-----+--------+--------+-------------+
        | row | letter | number | extra       |
        | --- | ------ | ------ | ----------- |
        | 0   | b      | 3      | partial     |
        | 1   | D      | 2      |             |
        | 2   | a      | 4      | unequal     |
        | 3   | c      | 20     |             |
        | 4   | C      | 10     | row-lengths |
        +-----+--------+--------+-------------+
    	`)

	testCases := sortingTestCasesFromTable(`
		| name                                  | skip               | sort  | columns      | exp-col | exp-rows           |
		| ------------------------------------- | ------------------ | ----- | ------------ | ------- | ------------------ |
		| no sort                               |                    | false |              |         | row 0 1 2 3 4      |
		| default sort                          |                    |       |              |         | row 0 1 2 3 4      |
		| reverse default sort                  |                    |       | ~            |         | row 4 3 2 1 0      |
		| implicit indexed column sort          |                    | false | letter       |         | row 0 1 2 3 4      |
		| indexed column sort                   |                    |       | letter       | 2       | letter a b c C D   |
		| reverse column sort                   |                    |       | ~letter      | 2       | letter D C c b a   |
		| third column default lexographic sort | default is numeric |       | number       | 3       | number 10 2 20 3 4 |
		| third column default numeric sort     |                    |       | number       | 3       | number 2 3 4 10 20 |
		| numeric sort                          |                    |       | #number      | 3       | number 2 3 4 10 20 |
		| reverse numeric sort                  |                    |       | ~#number     | 3       | number 20 10 4 3 2 |
		| numeric reverse sort                  |                    |       | #~number     | 3       | number 20 10 4 3 2 |
		| reverse reverse column sort           |                    |       | ~ #~number   | 3       | number 2 3 4 10 20 |
		| partial column sort                   |                    |       | extra letter | 2       | letter c D b C a   |
		`)

	runSortingTestCases(t, testTable, testCases)
}

func TestColumnNameMatching(t *testing.T) {

	testTable, _ := psv.TableFromString(`
        +-----+--------+--------+-------+
        | row | letter | number | name  |
        | --- | ------ | ------ | ----- |
        | 0   | x      | 5      | zero  |
        | 1   | x      | 11     | one   |
        | 2   | x      | 2      | two   |
        | 3   | y      | 5      | three |
        | 4   | y      | 11     | four  |
        | 5   | y      | 2      | five  |
        +-----+--------+--------+-------+
    	`)

	testCases := sortingTestCasesFromTable(`
		| name                                       | skip               | columns | exp-rows        | comments                            |
		| ------------------------------------------ | ------------------ | ------- | --------------- | ----------------------------------- |
		| no matching names                          |                    | xyz     | row 0 1 2 3 4 5 | unchanged                           |
		| no matching names, reversed                |                    | ~xyz    | row 0 1 2 3 4 5 | unchanged                           |
		| unique column name                         |                    | nam     | row 5 4 1 3 2 0 | 'name' only                         |
		| unique column name                         |                    | ~nam    | row 0 2 3 1 4 5 | 'name' only, reversed               |
		| ambiguous column name, lexographic default | default is numeric | n       | row 4 1 5 2 3 0 | 'name' then 'number'                |
		| ambiguous column name, numeric default     |                    | n       | row 5 2 3 0 4 1 | 'name' then 'number', numerically   |
		| ambiguous column name, lexographic         |                    | @n      | row 4 1 5 2 3 0 | 'name' then 'number'                |
		| ambiguous column name, numeric             |                    | #n      | row 5 2 3 0 4 1 | 'name' then 'number', numerically   |
		| ambiguous column name, reverse numeric     |                    | ~#n     | row 1 4 0 3 2 5 | 'name' then 'number", then reversed |
		| broad match, lexographic default           | default is numeric | e       | row 1 2 0 4 5 3 |                                     |
		| broad match, numeric default               |                    | e       | row 2 0 1 5 3 4 |                                     |
		| broad match, lexographically               |                    | @e      | row 1 2 0 4 5 3 |                                     |
		| broad match, numeric                       |                    | #e      | row 2 0 1 5 3 4 |                                     |
		| broad match, reverse numeric               |                    | #~e     | row 4 3 5 1 0 2 |                                     |
		`)

	runSortingTestCases(t, testTable, testCases)
}

func TestMultiSectionSorting(t *testing.T) {

	// warning: since the test cases are defined via strings.Fields()
	// (space separated), we can't test cells with spaces in their data.
	// i.e.: all data is space-less
	testTable, _ := psv.TableFromString(`
        Stuff:
        +-----+--------+--------+-------------+
        | row | letter | number | extra       |
        | --- | ------ | ------ | ----------- |
        | 0   | b      | 3      | partial     |
        | 1   | D      | 2      |             |
        | 2   | a      | 4      | unequal     |
        | 3   | c      | 20     |             |
        | 4   | C      | 10     | row-lengths |
        +-----+--------+--------+-------------+

        Names:
        +--------------+
        | row | Name   |
        +--------------+

        Parents:
        ----------------
        | 0   | Sandy  |
        | 1   | Andy   |

        Kids:
        | 2   | Max    |
        | 3   | Tim    |
        | 4   | Eva    |

        Pets:
        | 5   | Socks  |
        | 6   | Fluffy |
        | 7   | Red    |
        +--------------+

        Temperatures:
        | row | Date | Min | Max
        | -
        | 0 | 01 | -6 | -2
        | 1 | 02 | -12 | 5
        | 2 | 03 | -6 | 2
    	`)

	// basic assumption: rows are only sorted in their entirety
	testCases := sortingTestCasesFromTable(`
		| wip | name                                     | sort  | sections | columns   | exp-rows                                    | comments                                             |
		| --- | ---------------------------------------- | ----- | -------- | --------- | ------------------------------------------- | ---------------------------------------------------- |
		|     | no sort                                  | false |          |           | row 0 1 2 3 4 row 0 1 2 3 4 5 6 7 row 0 1 2 |                                                      |
		|     | no implicit sort via section index       | false | 6        |           | row 0 1 2 3 4 row 0 1 2 3 4 5 6 7 row 0 1 2 |                                                      |
		|     | no implicit sort via named section       | false | pets     |           | row 0 1 2 3 4 row 0 1 2 3 4 5 6 7 row 0 1 2 |                                                      |
		|     | default section sort                     |       |          |           | row 0 1 2 3 4 row 0 1 2 3 4 5 6 7 row 0 1 2 |                                                      |
		|     | default reverse section sort             |       | ~        |           | row 4 3 2 1 0 row 1 0 4 3 2 7 6 5 row 2 1 0 |                                                      |
		|     | default reverse section and column sort  |       | ~        | ~         | row 0 1 2 3 4 row 0 1 2 3 4 5 6 7 row 0 1 2 |                                                      |
		|     | sort within section by index             | false | 6        |           | row 0 1 2 3 4 row 0 1 2 3 4 5 6 7 row 0 1 2 | no effect - sorts all columns from left to right     |
		|     | sort within section by named             | false | pets     |           | row 0 1 2 3 4 row 0 1 2 3 4 5 6 7 row 0 1 2 | no effect - sorts all columns from left to right     |
		|     | reverse sort via section index           |       | ~6       |           | row 0 1 2 3 4 row 0 1 2 3 4 7 6 5 row 0 1 2 | only reverse order of rows in section 6 (pets)       |
		|     | reverse sort via named section           |       | ~pets    |           | row 0 1 2 3 4 row 0 1 2 3 4 7 6 5 row 0 1 2 |                                                      |
		|     | section index and column index           |       | 6        | 2         | row 0 1 2 3 4 row 0 1 2 3 4 6 7 5 row 0 1 2 | pets sorted by name                                  |
		|     | section index and column names           |       | 6        | name      | row 0 1 2 3 4 row 0 1 2 3 4 6 7 5 row 0 1 2 | ditto                                                |
		|     | section name and column index            |       | pets     | 2         | row 0 1 2 3 4 row 0 1 2 3 4 6 7 5 row 0 1 2 | ditto                                                |
		|     | section name and column names            |       | pets     | name      | row 0 1 2 3 4 row 0 1 2 3 4 6 7 5 row 0 1 2 | ditto                                                |
		|     | reverse section and column names         |       | 6        | ~name     | row 0 1 2 3 4 row 0 1 2 3 4 5 7 6 row 0 1 2 | pets sorted by name, backwards                       |
		|     | reverse section and column names         |       | pets     | ~name     | row 0 1 2 3 4 row 0 1 2 3 4 5 7 6 row 0 1 2 | ditto                                                |
		|     | reverse section and reverse column names |       | ~6       | ~name     | row 0 1 2 3 4 row 0 1 2 3 4 6 7 5 row 0 1 2 | pets sorted by names, backwards backward => forwards |
		|     | reverse section and reverse column names |       | ~pets    | ~name     | row 0 1 2 3 4 row 0 1 2 3 4 6 7 5 row 0 1 2 | ditto                                                |
		`)

	runSortingTestCases(t, testTable, testCases)
}

func TestNumericSorting(t *testing.T) {

	testTable, _ := psv.TableFromString(`
		| row | natural | leading | signed | float | mix  |
		| --- | ------- | ------- | ------ | ----- | ---- |
		| 0   | 4       | 00004   | +2     | 1.006 | 4.7  |
		| 1   | 2       | 002     | +1     | 1.0   | -6   |
		| 2   | 3       | 003     | -2     | 1.50  | -12  |
		| 3   | 1       | 00001   | 0      | 1.5   | +6   |
		| 4   | 5       | 05      | -1     | 1.08  | -3.2 |
		`)

	// hint: full list of bcp47 codes: https://en.wikipedia.org/wiki/IETF_language_tag
	testCases := sortingTestCasesFromTable(`
		| name                      | columns  | exp-rows      | skip                              |
		| ------------------------- | -------- | ------------- | --------------------------------- |
		| simple integers           | #natural | row 3 1 2 0 4 |                                   |
		| integers with leading 0's | #natural | row 3 1 2 0 4 |                                   |
		| signed integers           | #signed  | row 2 4 3 1 0 | signed integers don't work        |
		| floats                    | #float   | row 1 0 4 2 3 | floating point numbers don't work |
		`)

	runSortingTestCases(t, testTable, testCases)
}

func TestLanguageSpecificSorting(t *testing.T) {

	testTable, _ := psv.TableFromString(`
	    +-----+---------+---------+--------------------------------+
	    | row | latin   | utf-8   | comment                        |
	    | --- | ------- | ------- | ------------------------------ |
	    | 0   | Aaron   | Åron    | Aa comes after Z for the Danes |
	    | 1   | Alex    | Alex    |                                |
	    | 2   | Ben     | Ben     |                                |
	    | 3   | Carter  | Carter  |                                |
	    | 4   | Charles | Charles | Ch preceeds Ca for the Czech   |
	    | 5   | Daniel  | Daniel  |                                |
	    | 6   | Samuel  | Samuel  |                                |
	    +-----+---------+---------+--------------------------------+
    	`)

	// hint: full list of bcp47 codes: https://en.wikipedia.org/wiki/IETF_language_tag
	testCases := sortingTestCasesFromTable(`
		| wip | name                  | language | columns | exp-rows          |
		| --- | --------------------- | -------- | ------- | ----------------- |
		|     | latin sort            |          | latin   | row 0 1 2 3 4 5 6 |
		|     | localised sort        |          | utf     | row 1 0 2 3 4 5 6 |
		|     | danish latin sort     | da       | latin   | row 1 2 3 4 5 6 0 |
		|     | danish localised sort | da       | utf     | row 1 2 3 4 5 6 0 |
		|     | czech latin sort      | cs       | latin   | row 0 1 2 3 5 4 6 |
		|     | czech localised sort  | cs       | utf     | row 1 0 2 3 5 4 6 |
		`)

	runSortingTestCases(t, testTable, testCases)
}

//----------------------------------------------------------------------

type sortingTestCase struct {
	name     string
	comments string
	skip     string // don't expect this test to succeed - value should be a description of why not
	sort     bool
	language string // bcp47 language tag used to determine which collation rules to use
	sections string // space-separated list of sections to sort
	columns  string // space-separated list of columns to sort
	expCol   int    // 1-based column to check for expected values
	expRows  string // space-separated list of expected values
}

func sortingTestCasesFromTable(text string) []sortingTestCase {
	tbl, _ := psv.TableFromString(text)

	col := map[string]int{}
	stat := map[string]int{}
	cols := tbl.ColumnNames()
	if len(cols) == 0 {
		cols = strings.Fields(`name sort sections columns exp-col exp-rows comments`)
	}
	for i, n := range cols {
		col[n] = i
		stat[n] = 0
	}

	wip := []int{}
	testCases := make([]sortingTestCase, 0, len(tbl.Data)-1)
	for tn, row := range tbl.Data[1:] {
		tc := sortingTestCase{
			sort: true,
		}
		if col, ok := col[`wip`]; ok {
			if row[col] != "" {
				wip = append(wip, tn)
			}
			stat[`wip`]++
		}
		if col, ok := col[`skip`]; ok {
			if row[col] != "" {
				tc.skip = row[col]
			}
			stat[`skip`]++
		}
		if col, ok := col[`name`]; ok {
			tc.name = row[col]
			stat[`name`]++
		}
		if col, ok := col[`comments`]; ok {
			tc.comments = row[col]
			stat[`comments`]++
		}
		if col, ok := col[`sort`]; ok {
			tc.sort = (row[col] == `true` || row[col] == "")
			stat[`sort`]++
		}
		if col, ok := col[`language`]; ok {
			tc.language = row[col]
			stat[`language`]++
		}
		if col, ok := col[`sections`]; ok {
			tc.sections = row[col]
			stat[`sections`]++
		}
		if col, ok := col[`columns`]; ok {
			tc.columns = row[col]
			stat[`columns`]++
		}
		if col, ok := col[`exp-col`]; ok {
			tc.expCol, _ = strconv.Atoi(row[col])
			stat[`exp-col`]++
		}
		if col, ok := col[`exp-rows`]; ok {
			tc.expRows = row[col]
			stat[`exp-rows`]++
		}

		if tc.expCol == 0 {
			tc.expCol = 1
		}

		testCases = append(testCases, tc)
	}

	for colName, count := range stat {
		if count == 0 {
			panic(fmt.Sprintf("The %q column does not match any fields in struct sortingTestCase\n", colName))
		}
	}

	if len(wip) > 0 {
		wipCases := make([]sortingTestCase, 0, len(wip))
		for _, tn := range wip {
			wipCases = append(wipCases, testCases[tn])
		}
		return wipCases
	}

	return testCases
}

// runSortingTestCases runs a set of sortingTestCase unit tests against a specific table
func runSortingTestCases(t *testing.T, testTable *psv.Table, testCases []sortingTestCase) {
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if tc.skip != "" {
				t.Skip("SKIPPED " + tc.skip)
			}

			want := is.New(t)

			// Given
			// clone original testTable to prevent unexpected side-effects
			tbl := testTable.Clone()

			// When
			tbl.Options.Sort = tc.sort                             // turn sorting on/off
			tbl.Options.SortLanguage = tc.language                 // define which collation rules should be used for sorting
			tbl.Options.SortSections = strings.Fields(tc.sections) // define which sections should be sorted
			tbl.Options.SortColumns = strings.Fields(tc.columns)   // define which columns should be sorted
			tbl.Sort()                                             // actually sort the table's rows

			// Then
			expRows := strings.Fields(tc.expRows)
			ok := want.Equal(len(tbl.Data), len(expRows), "number of rows should be unchanged")
			c := tc.expCol - 1
			for r := range expRows {
				rowOK := want.Equal(tbl.Data[r][c], expRows[r], fmt.Sprintf("row %d should be %q", r, expRows[r]))
				ok = ok && rowOK // no &&= operator in go?!
			}

			if !ok {
				// turn off sorting so that Encode() doesn't re-sort() the table again (with side-effects)
				tbl.Options.Sort = false
				tbl.Options.SortSections = nil
				tbl.Options.SortColumns = nil
				t.Logf("unexpected table:\n%s\n", tbl.Encode())
			}

		})
	}
}
