/*

Package psv converts [][]string slices into pretty, pipe-separated text tables,
and vice-versa.

Example

    tbl := psv.New()
    tbl.SetIndent("    ")
    tbl.TextLines = []*psv.TextLine{
        {Text:`The "who's-who" list of people`},
        {},
        {Line:4,Ruler:"+ -"},
    }
    tbl.Data = [][]string{
        {"name", "age"},
        {"Joe",	"42"},
        {"Freddie", "41"},
        {"Amy",	"don't ask"},
    }
    fmt.Println(tbl.Encode())

Output

        The "who's-who" list of people

        | name    | age       |
        + ------- + --------- +
        | Joe     | 42        |
        | Freddie | 41        |
        | Amy     | don't ask |

The table data and appearance can be set up via any of the following examples:

    // convert a string into a table
    tbl := table.DecodeString(string)
    // all text lines are retained for re-rendering, e.g.
    fmt.Println(tbl.Encode())

    // create a table from scratch (string data only)
    tbl := table.NewTable()
    tbl.Indent = "..."
    tbl.TextLines = []*TextLine{...}
    tbl.Data = [][]string{...}
    fmt.Println(tbl.Encode())

    // optionally modify aspects of the table
    tbl.Indent = "..."
    tbl.TextLines = []*TextLine{...}
    fmt.Println(tbl.Encode())

References

This package focusses on data representation,
human readability and the exchange of intentions, which a computer may
incidentally be able to make sense of. Mostly, each cell of a table is a
simple, single string value, however, with a bit of additional work,
the string values may be mapped to slices or maps of slightly more
complicated (incl. custom) structures.

The lack of support for every last, complicated, nested structure is
intentional.

There a large number of csv, tsv, dsv packages availble on pkg.go.dev, but they
all seem to concentrate on "machine readable data exchange". Nevertheless, they
served as inspiration for this package as well.

psv always *felt* like a package I should not have to write, but I was unable
to find an existing program with suitable features:

- simple to use, suitable for as an editor plugin / shell pipe
- align columnated data
- while ignoring lines which aren't columnated

The unix tools [column] and [tbl] and go's own encoding/csv package all served
as a good basis, but they all follow different goals.

Basic Concepts

Table is the central struct provided by psv.
With a Table struct you can then add rows of data as well as text lines
(non-table rows to be interspersed before, between or after the data rows) and
additional formatting such as indents.

Creating Tables

Reading Tables

The table parser expects an bufio.Scanner, from which it will extract all rows of
data (beginning with a '|' character), while retaining enough information to
re-construct the original text it was given.

For convenience, psv.TableFromString() may be used to parse in-situ tables,
ideal for testing etc.

e.g.

    colors, _ := psv.TableFromString(`
                                    | color | rgb | hue |
                                    | ----- | --- | --- |
                                    | red   | f00 | 0   |
                                    | green | 0f0 | 120 |
                                    | blue  | 00f | 240 |
                                    `)

    for _, row := range colors.DataRows()[1:] { // [1:] skips the header row
        ...
    }

*/
package psv
