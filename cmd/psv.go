package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"runtime"
	"strings"

	"codeberg.org/japh/psv"
)

var (
	// use `make install` to have these set to useful values
	buildVersion = "<unknown>" // used to identify the version of each psv binary
	buildTime    = "<unknown>" // UTC timestamp of the time that the psv binary was built
)

func main() {
	flag.String("indent", "", "String to use to indent each line of the table (may also be an unsigned integer < 64)")
	flag.String("i", "", "String to use to indent each line of the table (may also be an unsigned integer < 64)")
	flag.Bool("squash", false, "Remove empty columns and reduce multiple empty rows to a single empty row")
	flag.Bool("s", false, "Sort table data - see also --by and --in")
	flag.Bool("sort", false, "Sort table data - see also --by and --in")
	flag.String("by", "", "Comma separated list of columns (name or number) to sort by. (implies --sort)")
	flag.String("in", "", "Comma separated list of sections (name or number) to sort within (all other sections are left unsorted) (implies --sort)")
	flag.Bool("version", false, "Show the current version and exit")
	flag.Bool("v", false, "Show the current version and exit")
	flag.Parse()

	tbl := psv.NewTable()

	// call visitOption() for each option that was detected on the command line
	flag.Visit(func(f *flag.Flag) { visitOption(tbl, f) })

	tbl.Read(bufio.NewScanner(os.Stdin))
	fmt.Print(tbl.Encode())

	os.Exit(0)
}

func visitOption(tbl *psv.Table, f *flag.Flag) {
	switch f.Name {
	case "i", "indent":
		err := tbl.SetIndent(f.Value.String())
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error: %v", err)
			os.Exit(1)
		}
	case "s", "sort":
		tbl.Options.Sort = true
	case "by":
		tbl.Options.SortColumns = strings.Split(f.Value.String(), ",")
		tbl.Options.Sort = true // the user probably wants sorting
	case "in":
		tbl.Options.SortSections = strings.Split(f.Value.String(), ",")
		tbl.Options.Sort = true // the user probably wants sorting
	case "squash":
		tbl.Options.Squash = true
	case "v", "version":
		fmt.Printf("psv version %s (%s %s built %s)\n", buildVersion, runtime.GOOS, runtime.GOARCH, buildTime)
		os.Exit(0)
	}
}
