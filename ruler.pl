#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;

my @letters = ( qw( | - + : = ), ' ' );
my @data = (
            # assumptions: a table without any columns... isn't a table
            # empty columns are possible, but then they also have no title ?!?
            # difficult: a table which only has single-character wide columns
            # normal: at least one column with width >=2 - easy!
            [ 0, 0, 0 ],  # very unlikely
            [ 0, 1, 2 ],
            [ 1, 1, 2 ],
            [ 1, 1, 1 ],  # very unlikely
            [ 1, 2, 1 ],
            [ 5, 0, 7 ],
            [ 1, 2, 3 ],
            [ 1, 1, 1, 5 ],
            [ 5, 1 ],
            );

my $roundtrip = {
                fmt => 0,
                rul => 0,
                tot => 0,
                };
foreach my $b ( @letters )
    {
    next if $b eq ' ';
    foreach my $p ( @letters )
        {
        next if $p =~ /[|:]/;
        foreach my $l ( @letters )
            {
            next if $l =~ /[|:]/;
            foreach my $s ( @letters )
                {
                my $fmt = join('', $b,$p,$l,$s);
                printf "\n%s\n", $fmt;
                my $ds = $s =~ /[|:]/ ? $s : '|';
                foreach my $c ( @data )
                    {
                    my $r0     = ruler->new($fmt);
                    my $r1     = ruler->new($r0->encode(@{$c}));
                    my $eq_fmt = $r1->format() eq $r0->format() ? 1 : 0;
                    my $eq_rul = $r1->encode(@{$c}) eq $r0->encode(@{$c}) ? 1 : 0;
                    my $yay    = ( $eq_fmt and $eq_rul ) ? ":-)"
                               : $eq_rul                 ? ":-|"
                               :                           ":-(";
                    printf "     => %s %s %s %s\n", $b, join(" $ds ", map { $_ x $_ } @{$c} ), $b, $yay;
                    printf "%-4s => %s %s\n", $fmt,          $r0->encode(@{$c}), $yay;
                    printf "%-4s => %s %s\n", $r1->format(), $r1->encode(@{$c}), $yay;
                    $roundtrip->{fmt}++ if $eq_fmt;
                    $roundtrip->{rul}++ if $eq_rul;
                    $roundtrip->{tot}++;
                    }
                }
            }
        }
    }

my @tests = (

            {
            line  => __LINE__,
            name  => 'empty format',
            then  => { format => '----', line => '-------------' },
            },

            {
            line  => __LINE__,
            name  => '1-character format',
            given => ':',
            then  => { format => '::::', line => ':::::::::::::' },
            },

            {
            line  => __LINE__,
            name  => '2-character format',
            given => ':-',
            then  => { format => ':--:', line => ':--:---:----:' },
            },

            {
            line  => __LINE__,
            name  => '3-character format',
            given => ': -',
            then  => { format => ': -:', line => ':  : - : -- :' },
            },

            {
            line  => __LINE__,
            name  => '4-character format',
            given => ': -+',
            then  => { format => ': -+', line => ':  + - + -- :' },
            },

            {
            line  => __LINE__,
            name  => 'decode ruler generated with 4 different characters',
            given => ': - + - + -- :',
            then  => { format => ': -+', line => ':  + - + -- :' },
            },

            {
            line  => __LINE__,
            name  => 'bad input: 5 different characters, do not generate ruler, just return input',
            given => ':=-+|',
            then  => {
                     format => ':=-+|',
                     line   => ':=-+|',
                     },
            },

            {
            line  => __LINE__,
            name  => 'bad input: 6 different characters',
            given => ':=-+*|',
            then  => {
                     format => ':=-+*|',
                     line   => ':=-+*|',
                     },
            },

            {
            line  => __LINE__,
            name  => 'alternating separator and padding characters',
            given => '| - | - | - |',
            when  => [ 1, 1, 1 ],
            then  => { format => '| -|', line => '| - | - | - |' },
            },

            {
            line  => __LINE__,
            name  => 'alternating separator and line characters',
            given => ': - + - + - :',
            when  => [ 0, 1, 1 ],
            then  => { format => ': -+', line => ':  + - + - :' },
            },

            {
            line  => __LINE__,
            given => ':  + - + -- :',
            then  => { format => ': -+', line => ':  + - + -- :' },
            },

            {
            line  => __LINE__,
            given => ':    : + - + -- :::',
            then  => {
                     format =>  ':    : + - + -- :::',
                     line   =>  ':    : + - + -- :::',
                     },
            },

            );

for my $test ( @tests )
    {
    my $given = $test->{given} // '';
    my $when  = $test->{when}  // [0,1,2];
    my $then  = $test->{then}  // {};
    my $name  = sprintf( "%s '%s' [ %s ] from line %d",
                        $test->{name} // "test",
                        $given,
                        join(',', @{$when}),
                        $test->{line},
                        );

    subtest $name,
            sub {
                my $ruler = ruler->new( $given );
                my $line = $ruler->encode( @{$when} );

                is( $ruler->format(), $then->{format}, "format" );
                is( $line,            $then->{line},   "line"   );
            }
    }

done_testing();

printf "roundtrip success rate: %d %% ok (%s bad formats + %d bad rulers / %d)\n",
       ( ( $roundtrip->{fmt} + $roundtrip->{rul} ) / 2 ) / $roundtrip->{tot} * 100,
       $roundtrip->{tot} - $roundtrip->{fmt},
       $roundtrip->{tot} - $roundtrip->{rul},
       $roundtrip->{tot};

ruler->dump_stats();

exit;

package ruler;

use strict;
use warnings;

my $stats;

sub new
    {
    my $class  = shift;
    my $format = shift;

    my $self = bless {}, $class;
    $self->decode( $format );

    return $self;
    }

sub format { shift->{format} }

sub bump
    {
    my $stat = shift;
    $stats //= {};
    $stats->{$stat}++;
    printf "bump: %s\n", $stat;
    }

sub dump_stats
    {
    use YAML;
    print Dump($stats);
    }

sub decode
    {
    my $self  = shift;
    my $input = shift;

    my @f = split( //, $input );

    if( @f > 4 )
        {
        @f = ();

        bump("split > 4");

        printf "parsing: '%s'\n", $input;

        # remove first and last 2 characters - must be left border and padding
        my ( $b, $p, $content ) = $input =~ /^(?<b>.)(?<p>.)(.*?)(?:\g{p}\g{b})?$/;
        push @f, $b, $p;

        my @chgrps = ( $content =~ /(?<x>.)(\g{x}*)/g );

        my $dup = {};
        my @unique = grep { not $dup->{$_}++ } split( '', $content );
        printf " unique: <%s>\n", join( "> <", @unique );

        my $unique_count = @unique;

        if( @unique >= 4 )
            {
            # @unique >= 4
            #   only when format is incorrect and not a generated ruler
            bump("unique >= 4");
            printf "sequence <%s> makes no sense\n", $input;
            $self->{format} = $input;
            return;
            }

        if( @unique == 3 )
            {
            # @unique == 3
            #   when 1st column has width >= 1 and padding != line != sep
            # or when a bad combination was provided
            #
            # When parsing a generated ruler
            #   only possible when pad != line != sep
            #   generated combinations:
            #       ( line, pad, sep )  when width of 1st col >= 0  e.g.: | (+ : +) |
            #       ( pad, sep, line )  when width of 1st col == 0  e.g.: | ( : +) |
            #
            bump("unique == 3");
            if( $unique[1] eq $f[1] )
                {
                # first column has >0 width
                bump("reduce line (pad) sep");
                push @f, @unique[0,2];
                # @unique = @unique[0,2];
                # # fall-through to @unique == 2
                }
            elsif( $unique[0] eq $f[1] )
                {
                bump("reduce (pad) sep line");
                push @f, @unique[2,1];
                # @unique = @unique[2,1];
                # # fall-through to @unique == 2
                }
            else
                {
                printf "sequence <%s> makes no sense\n", $input;
                $self->{format} = $input;
                return;
                }
            }

        if( @unique == 2 )
            {
            # @unique == 2
            #   1st col width > 0
            #       most likely
            #       pad  == line != sep         (line/pad, sep)  e.g.: | (  :  ) |
            #       pad  == sep  != line        (line, pad/sep)  e.g.: | (+   +) |
            #       line == sep  != padding     (line/sep, pad)  e.g.: | (: : :) | => sep <- line
            #   1st col width == 0
            #       unlikely
            #       undetectable without context (e.g. from previous data rows)
            #       pad  == line != sep         (pad/line, sep)  e.g.: | ( :  ) |
            #       pad  == sep  != line        (pad/sep,  line) e.g.: | (   +) | => swap sep <-> line
            #       line == sep  != padding     (pad,  sep/line) e.g.: | ( : :) | => line <- sep
            bump("unique == 2");
            my @attempts = (
                            [@f, @unique[0,1]],
                            [@f, @unique[0,0]],
                            [@f, @unique[1,0]],
                            [@f, @unique[1,1]],
                            );
            foreach my $i ( 0 .. $#attempts )
                {
                my @attempt = @{$attempts[$i]};
                if( is_plausible( $input, @attempt ) )
                    {
                    bump("attempt $i");
                    @f = @attempt;
                    last;
                    }
                }
            }

        if( @unique == 1 )
            {
            # @unique == 1
            #   when pad == line == sep
            #     or pad == line and only have 1 column
            #     or pad == sep  and only have columns with 0 width
            # => assume 1 column only
            #   => fall back to defaults:
            #       line <= padding
            #       sep  <= border
            bump("unique == 1");
            push @f, $unique[0], $unique[0];
            }

        }

    push( @f, '-'   ), bump( "default 0" ) if @f == 0;
    push( @f, $f[0] ), bump( "default 1" ) if @f == 1;
    push( @f, $f[1] ), bump( "default 2" ) if @f == 2;
    push( @f, $f[0] ), bump( "default 3" ) if @f == 3;

    $self->{format} = join( '', @f[0..3] );

    return;
    }

sub is_plausible
    {
    my ( $input, $b, $p, $l, $s ) = @_;
    printf "trying %s\n", join('', $b, $p, $l, $s );
    my $pat = sprintf "^%s%s%s*?(%s%s%s%s*?)*%s%s\$", map { quotemeta } $b, $p, $l, $p, $s, $p, $l, $p, $b;
    return $input =~ /$pat/;
    }

sub encode
    {
    my $self = shift;
    my $cols = @_;

    return $self->{format} if length( $self->{format} ) > 4;

    my ( $b, $p, $l, $s ) = split( '', $self->{format} );

    my @l = $b;
    foreach my $c ( @_ )
        {
        push @l, $p;
        push @l, $l x $c;
        push @l, $p;
        push @l, $s;
        }
    pop @l if @l > 1;
    push @l, $b;

    return join("",@l);
    }
