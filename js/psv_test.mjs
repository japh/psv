import { tableFromString } from "./psv.mjs"

let passed = 0
let failed = 0

{
    const tbl = tableFromString()
    const ok  = isEqualTable( tbl, `no parameters`, [], [] )
    if( ok ) { passed++ }
    else     { failed++ } 
}

const testCases = [
    { abstract: `empty string`,                 psv: ``,               headers: [], data: [], },
    { abstract: `one pipe, no table`,           psv: `|`,              headers: [], data: [], },
    { abstract: `two pipes, no table`,          psv: `||`,             headers: [], data: [], },
    { abstract: `text only, no table`,          psv: `one`,            headers: [], data: [], },
    { abstract: `text before pipe, no table`,   psv: `one`,            headers: [], data: [], },
    { abstract: `pipe before text, one cell`,   psv: `|one`,           headers: [], data: [[ `one`        ]], },
    { abstract: `text between pipes, one cell`, psv: `|one|`,          headers: [], data: [[ `one`        ]], },
    { abstract: `multiple columns`,             psv: `|one|two`,       headers: [], data: [[ `one`, `two` ]], },
    { abstract: `padded data`,                  psv: `| one  | two |`, headers: [], data: [[ `one`, `two` ]], },

    { abstract: `multi-line`,
      psv:      `
          | one  | two |
          `,
          headers:  [],
          data:     [[`one`,`two`]],
    },

    { abstract: `multi-rows`,
      psv:      `
          | one   | two  |
          | three | four |
          `,
          headers:  [],
          data:     [
          [ `one`,  `two`  ],
          [ `three`,`four` ],
          ],
    },

    { abstract: `header & data`,
      psv:      `
          | one   | two  |
          | ----- | ---- |
          | three | four |
          `,
          headers:  [[ `one`,   `two`  ]],
          data:     [[ `three`, `four` ]],
    },

    { abstract: `header & multi-data`,
      psv:      `
          | one   | two  |
          | ----- | ---- |
          | three | four |
          | five  | six  |
          `,
          headers:  [[ `one`,   `two`  ]],
          data:     [
          [ `three`, `four` ],
          [ `five`,  `six`  ],
          ],
    },

    { abstract: `multi header & data`,
      psv:      `
          | one   | two  |
          | three | four |
          | ----- | ---- |
          | five  | six  |
          `,
          headers:  [
          [ `one`,   `two`  ],
          [ `three`, `four` ],
          ],
          data:     [
          [ `five`,  `six`  ],
          ],
    },

    { abstract: `rulers are ignored`,
      psv:      `
          | one   | two  |
          | ----- | ---- |
          | three | four |
          | ----- | ---- |
          | five  | six  |
          `,
          headers:  [[ `one`,   `two`  ]],
          data:     [
          [ `three`, `four` ],
          [ `five`,  `six`  ],
          ],
    },

    { abstract: `different ruler`,
      psv:      `
          | one   | two  |
          + ----- | ---- +
          | three | four |
          `,
          headers:  [[ `one`,   `two`  ]],
          data:     [[ `three`, `four` ]],
    },

    { abstract: `extra decoration`,
      psv:      `
          +-------+------+
          | one   | two  |
          +=======+======+
          | three | four |
          +-------+------+
          `,
          headers:  [[ `one`,   `two`  ]],
          data:     [[ `three`, `four` ]],
    },

    { abstract: `inconsistent table width`,
      psv:      `
          | one |
          + --- + ----- +
          | two | three |
          `,
          headers:  [[ `one`, ``      ]],
          data:     [[ `two`, `three` ]],
    },

    { abstract: `additional data column`,
      psv:      `
          | one  |
          + ---- + ----- +
          | two  | three | four
          | five | six   |
          `,
          headers:  [[ `one`,  ``,      ``    ]],
          data:     [
          [ `two`,  `three`, `four` ],
          [ `five`, `six`,   ``     ],
          ],
    },

    { abstract: `all features`,
      psv:      `

          Some Weather Statistics I tracked:

          | Day | Low | High | Description |
          +-----+-----+------+-------------+
          | Mon | -1  | +10 | mostly sunny
          | Tue | +5  | +9   | cloudy      | except it rained when I went outside
          | Wed | +10  | +20 | storm |
          | --- | | | | I was sick
          | Sat | -2 | +7 | windy

          `,
          headers:  [[ `Day`, `Low`, `High`, `Description`, ``                                    ]],
          data:     [
          [ `Mon`, `-1`,  `+10`, `mostly sunny`, ``                                     ],
          [ `Tue`, `+5`,  `+9`,  `cloudy`,       `except it rained when I went outside` ],
          [ `Wed`, `+10`, `+20`, `storm`,        ``                                     ],
          [ `---`, ``,    ``,    ``,             `I was sick`                           ],
          [ `Sat`, `-2`,  `+7`,  `windy`,        ``                                     ],
          ],
    },

    ]

    for( const tc of testCases ) {
        const tbl = tableFromString( tc.psv )
        const ok  = isEqualTable( tbl, tc.abstract, tc.headers, tc.data )
        if( ok ) { passed++ }
        else     { failed++ }
    }

if( failed ) { console.log( `FAIL: ${failed} tests failed, ${passed} tests passed` ) }
else         { console.log( `PASS: ${passed} tests passed`                         ) }

//////////
//
// Utility Functions
//

function isEqualTable( gotTable, abstract, wantHeaders, wantData ) {
    if( !isEqualRows( gotTable.headers, wantHeaders, `header` ) ) return false
    if( !isEqualRows( gotTable.data,    wantData,    `data`   ) ) return false
    return true
}

function isEqualRows( gotRows, wantRows, msg ) {
    if( !gotRows && !wantRows ) {
        return true
    }

    if( !( gotRows && wantRows ) ) {
        console.error( `fail: unexpected ${msg}\ngot:  ${gotRows}\nwant: ${wantRows}` )
        return false
    }

    if( gotRows.length != wantRows.length ) {
        console.error( `fail: unexpected number of ${msg} rows\ngot:  ${gotRows.length}\nwant: ${wantRows.length}` )
        return false
    }
    for( let r = 0; r < gotRows.length; r++ ) {
        const gotRow  = gotRows[r]
        const wantRow = wantRows[r]
        if( gotRow.length != wantRow.length ) {
            console.error( `fail: unexpected number of ${msg} cells in row ${r+1}\ngot:  ${gotRow.length}\nwant: ${wantRow.length}` )
            return false
        }
        for( let c = 0; c < gotRow.length; c++ ) {
            const gotCell  = gotRow[c]
            const wantCell = wantRow[c]
            if( gotCell != wantCell ) {
                console.error( `fail: unexpected ${msg} data at ${r+1},${c+1}\ngot:  ${gotCell}\nwant: ${wantCell}` )
                return false
            }
        }
    }
    return true
}
