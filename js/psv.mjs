//
// psv.tableFromString() is a minimal PSV parser for javascript
//
// The input string is expected to be a multi-line string containing
// pipe-separated data, for example:
//
//  import { tableFromString } from "./psv.mjs"
//
//  const rows = tableFromString(`
//
//      Some Weather Statistics I tracked:
//
//      | Day | Low | High | Description |
//      +-----+-----+------+-------------+
//      | Mon | -1  | +10 | mostly sunny
//      | Tue | +5  | +9   | cloudy      | but it rained when I went outside
//      | Wed | +10  | +20 | storm |
//      | --- | | | | I was sick
//      | Sat | -2 | +7 | windy
//
//  `)
//
//  would return:
//
//    rows: [
//          [ 'Day', 'Low', 'High', 'Description',  ''                                  ],
//          [ 'Mon', '-1',  '+10',  'mostly sunny', ''                                  ],
//          [ 'Tue', '+5',  '+9',   'cloudy',       'but it rained when I went outside' ],
//          [ 'Wed', '+10', '+20',  'storm',        ''                                  ],
//          [ '---', '',    '',     '',             'I was sick'                        ],
//          [ 'Sat', '-2', '+7',    'windy',        ''                                  ],
//          ]
//
// Some things to note:
//
//  - only lines that begin with a '|' are part of the table
//  - rulers (lines containing only '|', '+', '-', '=' or whitespace) are also ignored
//  - all rows are guaranteed to have the same number of cells
//  - all data is returned as strings - it's your data, you work out what to do with it
//  - I don't like the '.mjs' suffix, but nodeJS pushed me, convenience over style :-(
//
// Copyright 2023 Stephen Riehm
//
export function tableFromString( psv = "" ) {
    const rows    = []
    let dataStart = 0
    let width     = 0
    let adjust    = 0
    for( const line of psv.split( /\n/g ) ) {

        if(  line.match( /^\s*([|+][\s-=+|]*)*$/ ) ) {
            // skip rulers, e.g.: |----|-----|
            // the first ruler is used to separate the header rows from data rows
            dataStart ||= rows.length
            continue
        }

        if( !line.match( /^\s*\|/ ) ) {
            // skip non table lines
            continue 
        }

        const cells = line.split( /\s*\|\s*/g )

        // the first split is always just the indent
        cells.shift()

        // remove the last split if it is empty
        // i.e. table has leading and trailing '|'s
        if( cells[cells.length-1] === "" ) {
            cells.pop()
        }

        // work out how wide this table is
        if( cells.length > width ) { width = cells.length; adjust++ }

        // add the row
        rows.push( cells )
    }

    // make all rows the same width if necessary
    if( adjust > 1 ) {
        for( const row of rows ) {
            while( row.length < width ) {
                row.push( "" )
            }
        }
    }

    // if the first ruler appeared after the last data row, then all rows are
    // data rows
    if( rows.length == dataStart ) {
        dataStart = 0
    }

    return {
           headers: rows.slice(0,dataStart) ?? [],
           data:    rows.slice(dataStart)   ?? [],
           }
}
