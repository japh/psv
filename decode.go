package psv

import (
	"regexp"
)

type decoder struct {
	*Table            // reference to the table we're building
	line   int        // current input line number
	data   [][]string // buffer for collecting data, row by row
}

// Scanner is the interface required to read an input source, 1 line at a time.
// Subset of bufio.Scanner
type Scanner interface {
	Scan() bool
	Text() string
}

var isTableRowRE = regexp.MustCompile(`^[\s\pZ]*\|`)
var rowSplitRE = regexp.MustCompile(`[\s\pZ]*\|[\s\pZ]*`)
var stripIndentRE = regexp.MustCompile(`^[\s\pZ]*`)
var stripTrailingSpaceRE = regexp.MustCompile(`[\s\pZ]*$`)

// Read reads and decodes text lines from a Scanner which provides individual
// lines from some text source.
func (t *Table) Read(in Scanner) error {
	d := &decoder{
		Table: t,
	}

	for in.Scan() {
		d.decodeLine(in.Text())
	}

	// only after we have all rows can we ensure that the data rows have a
	// consistent size
	d.normaliseRows()

	return nil
}

func (d *decoder) decodeLine(text string) {
	d.line++

	// TODO(Steve) regex ReplaceAllString() returns a copy of the string
	// here, we really only want to ignore trailing white-spaces,
	// but there are so many that it is easier (for me, now) to rely on
	// regex's [\s\pZ] pattern
	text = stripTrailingSpaceRE.ReplaceAllString(text, "")
	indent := d.Table.FindIndent(text)
	content := text[len(indent):] // remove the indent from the text line

	switch {
	case isRuler(content):
		d.addRuler(content)
		d.Table.FinalizeIndent() // use indent of first data row for the entire table
	case isTableRowRE.MatchString(content):
		row := d.parseTableRow(content)
		d.data = append(d.data, row)
		d.Table.FinalizeIndent() // use indent of first data row for the entire table
	default:
		d.addTextLine(text)
	}
}

func (d *decoder) addRuler(text string) {
	d.Table.TextLines = append(d.Table.TextLines, &TextLine{Line: d.line, Ruler: text})
}

func (d *decoder) addTextLine(text string) {
	d.Table.TextLines = append(d.Table.TextLines,
		&TextLine{
			Line:   d.line,
			Indent: "",
			Text:   text,
		})
}

func (d *decoder) parseTableRow(text string) []string {
	row := rowSplitRE.Split(text, -1)

	// trim trailing empty cells
	c := len(row)
	for c > 1 && row[c-1] == "" {
		c--
	}

	return row[1:c]
}

func (d *decoder) normaliseRows() {
	rows := len(d.data)
	cols := 0
	for _, row := range d.data {
		if cols < len(row) {
			cols = len(row)
		}
	}

	d.Table.Data = make([][]string, rows)
	for r, row := range d.data {
		d.Table.Data[r] = make([]string, cols)
		for c, cell := range row {
			d.Table.Data[r][c] = cell
		}
	}
}
